# Cascading Stylesheets

## CSS Font Weights

The "font-weight" attribute defines the thickness of the font in CSS. Some fonts can only be displayed in bold or normal. Other fonts provide a more precise definition with a numeric value.

The following list shows the numerical value for the appropriate keyword:

- **100** Thin, Hairline, Ultra-light
- **200** Extra-light
- **300** Light, Book
- **400** Regular, Normal, Plain, Roman, Standard
- **500** Medium
- **600** Semi-bold, Demi-bold
- **700** Bold
- **800** Heavy, Extra-bold
- **900** Black, Ultra-black, Extra-black, Ultra-bold, Heavy-black, Fat, Poster

## Default Font-Size of Headings

```css
h1 { font-size: 200%; }
h2 { font-size: 150%; }
h3 { font-size: 120%; }
h4 { font-size: 100%; }
h5 { font-size: 80%; }
h6 { font-size: 70%; }
```


## CSS Flip-In Animation

Here is a little flip-in animation that I like to use in dropdowns at my navigations.

````css
selector {
	animation: flip-rotation 500ms ease-in-out forwards;
}

@keyframes flip-rotation {
	0% {
		opacity: 0;
		transform: translateY(45px) rotate3d(1, 1, 0, 45deg);
	}
    
	100% {
		opacity: 1;
		transform: translateY(0);
	}
}
````

## Detect iOS Device

````css
@supports (-webkit-touch-callout: none) {
	/* CSS specific to iOS devices */
}

@supports not (-webkit-touch-callout: none) {
	/* CSS for other than iOS devices */
}
````

## Smooth max-height Transition

````css
.collapse {
	max-height: 0;
	overflow: hidden;
	transition: max-height 500ms cubic-bezier(0, 1, 0, 1);
}

.expand {
	max-height: 999px;
	transition: max-height 1000ms ease-in-out;
}
````

## SVG in background-image with Color

You can simply put SVG Code in `background-image` property of your CSS class. Actually you can use the `fill` attribute now, but you have to be careful because in the `background-image` property the hash will not be recognized. So, you must encode them like this:

````css
selector {
	background-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewox="0 0 128 128" fill="%23ffffff"></svg>');
}
````

## Self-Hosted Fonts

````css
@font-face {
  font-family: 'Custom';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url('/assets/fonts/custom-400.woff') format('woff'), url('/assets/fonts/custom-400.woff2') format('woff2');
}
````

## Skeleton Loading

````css
selector {
  background: linear-gradient(90deg, #DEDEDE 40%, #EDEDED 50%, #DEDEDE 60%) 100% 50% / 300% 100% no-repeat;
  animation: skeleton-loading 900ms ease-in-out infinite;
}

@keyframes skeleton-loading {
  0% {
    background-position: 100% 50%;
  }

  100% {
  	background-position: 0 50%;
  }
}
````

## Hide Scrollbar on all Devices

```css
selector {
	overflow: scroll;
	scrollbar-width: none;
}

selector::-webkit-scrollbar {
	display: none;
}
```

## Polygon Background

```css
selector {
	padding: 128px 64px 128px 128px;
	background-color: #F00;
	clip-path: polygon(10% 10%, 100% 0, 100% 100%, 0 90%);
}
```

## Disable Horizontal Swipe Navigation on MacOS (Two-Finger Swipe for Back-/Forwards)

```css
body,
html {
	overscroll-behavior-x: none;
}
```
s