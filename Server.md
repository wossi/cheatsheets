# Server

## Force SSL to www

````
RewriteEngine On

RewriteCond %{HTTPS} !=on
RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

RewriteCond %{HTTP_HOST} !^www\. [NC]
RewriteRule ^(.*)$ http://www.%{HTTP_HOST}/$1 [R=301,L]
````

## Force SSL to non-www

````
RewriteEngine On
RewriteBase /

RewriteCond %{HTTP_HOST} ^(www\.)(.+) [OR]
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} ^(www\.)?(.+)
RewriteRule ^ https://%2%{REQUEST_URI} [R=301,L]
````

## Redirect all URLs to another domain

````
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteRule ^(.*)$ https://www.domain.com/$1 [R=302,L]
</IfModule>
````

## Hide PHP file extension

````
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^([^\.]+)$ $1.php [NC,L]
````

## Show content of subfolder

````
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ /folder/index.html [L,QSA]
````

## How to create a `*.pem` file

````
-----BEGIN RSA PRIVATE KEY-----
(Private Key: private.key contents)
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
(Primary SSL certificate: certificate.crt contents)
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
(Intermediate certificate: ca_bundle.crt contents)
-----END CERTIFICATE----
````

## Convert a `*.pem` file to `*.pfx` file

````
openssl pkcs12 -export -out xxx.pfx -in xxx.pem
````

## Change PHP Version for SSH on Server

Go to user’s home directory and create new file `.bash_profile` with the following
content:

````
PATH=/opt/plesk/php/7.2/bin:$PATH
````

## Set Environment Variable

````
SetEnv TYPO3_CONTEXT Development
````

## Block IP Address

If you have problems with DDoS or Brute-Force attacks and want to disable the connections quickly, use the following code in the `.htaccess` file. You can now make your adjustments and security measures at your leisure and remove the code afterwards.

````
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REMOTE_ADDR} !^127.0.0.1$
    RewriteRule ^(.*)$ - [R=403,L]
</IfModule>
````

## Block User-Agent

If you want to block a user-agent use this snippet.

```
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTP_USER_AGENT} "Go-http-client/1.1" [NC]
    RewriteRule .* - [F]
</IfModule>
```

## Allow IP Address

You can block your website and only allow visiting from specific IP address with the following code snippet.

```
Order deny,allow
Deny from all
Allow from 127.0.0.1
```
