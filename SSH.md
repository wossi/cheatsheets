# Secure Shell
## Get Database with SSH

Open terminal in PHPStorm and type the following line:
```bash
ssh USER@DOMAIN.COM
```

with GZIP...
```bash
mysqldump -u DB_USER -p DB_NAME | gzip > FILENAME.sql.gz
```

without GZIP....
```bash
mysqldump -u DB_USER -p DB_NAME > FILENAME.sql
```
### Troubleshooting

Error: 'Access denied; you need (at least one of) the PROCESS privilege(s) for this operation' when trying to dump tablespaces
```bash
mysqldump --no-tablespaces -u DB_USER -p DB_NAME > FILENAME.sql
```

## Put Database with SSH

Open terminal and type the following lines:

```bash
ssh USER@DOMAIN.COM
```

Upload database backup on your server. Connect with SSH and go to the directory where is the backup file located.
(Please, don't put *.sql files in ````/public````!)

Prepare your database password and run the following command...

```bash
mysql -u DB_USER -p DB_NAME < FILENAME.sql
```

## RSync with SSH

### Upload

Change to the directory that is to be uploaded and execute the following command in the terminal

```bash
rsync -ave ssh . user@domain.com:/upload_path/
```

### Download

Change to the directory where the download should go and execute the following command in the terminal.

```bash
rsync -ave ssh USER@DOMAIN.COM:/home/user/source/ .
```

### Exclude Files or Folder from Download

To exclude files use the following command:

```bash
rsync -ave ssh --exclude 'file.txt' ssh user@domain.com:/home/user/source/ .
rsync -ave ssh --exclude 'folder' ssh user@domain.com:/home/user/source/ .
```

### Local RSync

```bash
rsync -av ../relative_source_path/ .
```

## Read disk space with SSH

```bash
du -hs folder
```

## Create Symbolic Link

```bash
ln -s ./src/releases/current/public public
```

