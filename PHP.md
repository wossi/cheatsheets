# PHP

## Connection to MySQL Database

````php
define('DB_HOST', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', '');

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if ($mysqli->connect_error) {
    die('Connection failed: ' . $mysqli->connect_error);
}

$results = $mysqli->query('SELECT * FROM table');

if ($results->num_rows > 0) {
    while ($row = $results->fetch_assoc()) {
        print_r($row);
    }
}

$mysqli->close();
````

## Human Readability Time Function

You need a function to publish your date like "1 hour ago", "3 days ago" or something like that? No problem. Here is my way to do that.

````php
function ws_format_time($datetime, $full = false)
{
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;
	$diff = (array) $diff;

	$string = [
		'y' => 'year',
		'm' => 'month',
		'w' => 'week',
		'd' => 'day',
		'h' => 'hour',
		'i' => 'minute',
		's' => 'second',
	];

	foreach ($string as $key => $value) {
		if ($diff[$key]) {
			$prefix = 's';
			$string[$key] = $diff[$key] . ' ' . $value . ($diff[$key] > 1 ? $prefix : '');
		} else {
			unset($string[$key]);
		}
	}

	if (!$full) {
		$string = array_slice($string, 0, 1);
	}

	return $string ? implode(', ', $string) . ' ago' : 'just now';
}
````

## Time Localization (German Language)

````php
setlocale(LC_TIME, 'de_DE.utf8');
````

## Build CSV with PHP

````php
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="export-' . time() . '.csv"');
````

