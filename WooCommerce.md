# Search by SKU

```php
add_filter('posts_search', function ($search, $query_vars) {
  global $wpdb;
  
  if (isset($query_vars->query['s']) && !empty($query_vars->query['s'])) {
    $args = [
      'posts_per_page' => -1,
      'post_type' => 'product',
      'meta_query' => [
        [
          'key' => '_sku',
          'value' => $query_vars->query['s'],
          'compare' => 'LIKE',
        ],
      ],
    ];
    
    $posts = get_posts($args);
    
    if (empty($posts)) {
      return $search;
    }
    
    $get_post_ids = [];
    
    foreach ($posts as $post) {
      $get_post_ids[] = $post->ID;
    }
    
    if (sizeof($get_post_ids) > 0) {
      $search = str_replace('AND (((', "AND ((({$wpdb->posts}.ID IN (" . implode(',', $get_post_ids) . ")) OR (", $search);
    }
  }
  
  return $search;
}, 99, 2);
```
# Hide Orders in Customer Account (My Account)

```php
add_filter('woocommerce_my_account_my_orders_query', function ($args) {
    $statuses = wc_get_order_statuses();

    unset($statuses['wc-on-hold']); // wc-completed, wc-processing, etc.
    $args['status'] = array_keys($statuses);

    return $args;
}, 99);
```
# Rename Order Status

```php
add_filter('wc_order_statuses', function ($order_statuses) {
    foreach ($order_statuses as $key => $status) {
        if ('wc-on-hold' === $key) {
            $order_statuses['wc-on-hold'] = _x('my custom label', 'Order status', 'domain');
        }
    }

    return $order_statuses;
});
```
# Remove Order Status on Backend Order

```php
add_filter('wc_order_statuses', function ($order_statuses) {
  global $pagenow;
  
  $orderStates = [
    'wc-pending',
    'wc-checkout-draft',
    'wc-invoice-payment',
    'wc-processing',
    'wc-failed',
  ];
  
  if ($pagenow === 'post.php' && isset($_GET['post']) && $_GET['action'] == 'edit' && get_post_type($_GET['post']) === 'shop_order') {
    foreach ($orderStates as $orderState) {
      unset($order_statuses[$orderState]);
    }
  }
  
  return $order_statuses;
}, 20, 1);
```
# Get Product Attributes and Terms (for Filters)

```php
$productAttributes = wc_get_attribute_taxonomies();  
  
foreach ($productAttributes as $productAttribute) {
    $productAttributeValues = get_terms(['taxonomy' => 'pa_' . $productAttribute->attribute_name]);
}
```
# Child Theme for E-Mail Templates

Create a new folder like `hello-elementor-child` in `wp-content/themes` and add a new file `style.css` in here. The content for the `style.css`

```css
/**
 * Theme Name: Hello Elementor Child
 * Template: hello-elementor
 */
```

Additionally, you can copy the `screenshot.png` from your main theme. Now, you can copy the email templates of WooCommerce and edit them.

## Add Custom Checkbox to WooCommerce Checkout

```php
add_action('woocommerce_review_order_before_submit', function () {
	woocommerce_form_field('custom_checkbox', [
        'type' => 'checkbox',
        'label' => __('Custom Checkbox', 'domain'),
        'required' => true, // optional
    ], WC()->checkout->get_value('custom_checkbox'));
});
 
add_action('woocommerce_checkout_process', function () {
    if (!(int)isset($_POST['custom_checkbox'])) {
        wc_add_notice(__('Custom Checkbox Error'), 'error');
    }
});
 
add_action('woocommerce_checkout_update_order_meta', function ($order_id) {
    if (!empty($_POST['custom_checkbox'])) {
        update_post_meta($order_id, 'custom_checkbox', $_POST['my_custom_checkbox']);
    }
});
 
add_action('woocommerce_admin_order_data_after_billing_address', function ($order) {
    $customCheckbox = get_post_meta($order->get_id(), 'custom_checkbox', true);
 
    if ('1' === $customCheckbox) {
        _e('Custom Checkbox', 'domain');
    }
});
```
