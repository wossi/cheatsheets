# GIT

## GIT Clone

Create a project in PHPStorm like "PhpstormProjects/customer". After that, you can open the terminal and go to the customer directory:

````
rm -rf .idea && git clone git@project_url .
````

## Commit and Push

Go to the GIT directory in terminal...

````
git add .
git commit -m "my commit message here"
git push origin master
````

