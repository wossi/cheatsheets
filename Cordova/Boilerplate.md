# Template

`index.html`

```html
<!DOCTYPE html>  
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, viewport-fit=cover"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <title>My App</title>
</head>
<body>
<main></main>
<script src="cordova.js"></script>
<script src="js/script.js"></script>
</body>
</html>
```

`style.css`

```css
@import "_fonts.css";
@import "_variables.css";
@import "_defaults.css";
@import "_animations.css";

@import "_app.css";
```

`_defaults.css`

```css
*,  
*:before,  
*:after {  
    box-sizing: border-box;  
    outline: 0;  
    -webkit-tap-highlight-color: rgba(0,0,0,0);  
}  
  
html,  
body {  
    position: relative;  
    width: 100%;  
    height: 100%;  
    margin: 0;  
    padding: 0;  
    background-color: #000;  
    font-family: monospace;  
    font-weight: normal;  
    font-size: 10px;  
    color: #fff;  
    overflow: hidden;
    user-select: none; 
    -webkit-user-select: none; 
    -webkit-touch-callout: none;  
    -webkit-text-size-adjust: none;
}
```