# Build Apps with VueJS and Cordova

## 1. Install Packages

```bash
npm install -g cordova
npm install -g @vue/cli
```

## 2. Create Project

```bash
vue create com.demo.app

cd com.demo.app
vue add cordova
```

## 3. Serve and Build

```bash
cd com.demo.app

# preview build in browser
npm run cordova-serve-browser

# preview build on android
npm run cordova-serve-android

# build app package bundle for android
npm run cordova-build-android
```

### 3.1 Run App on Device via USB

```bash
cd com.demo.app/src-cordova

adb devices
cordova run android
```
