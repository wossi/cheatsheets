# Advanced Settings for Android

#### Lock Screen Orientation

```xml
<platform name="android">
	<preference name="Orientation" value="landscape" />
</platform>
```
#### Fullscreen (Hide Navigation and Status Bar)

```xml
<platform name="android">
	<preference name="Fullscreen" value="true" />
</platform>
```