# Splash Screen for Android

Grab the default icon (192x192) and place it in a larger document (288x288).

```xml
<platform name="android">  
    <preference name="AndroidWindowSplashScreenBackground" value="#000000" />  
    <preference name="AndroidWindowSplashScreenAnimatedIcon" value="resources/icon-splash-screen.png" />
</platform>
```
