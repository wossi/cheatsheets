## Install Cordova

```bash
npm install -g cordova
```
## Create New Project

```bash
cordova create demo com.app.demo Demo
cd demo
cordova platform add android
```