# App Icon for Android

The default icon size is 192x192 pixels.

```xml
<platform name="android">
	<icon src="resources/icon-app.png" />
    <!--
        ldpi    : 36x36 px
        mdpi    : 48x48 px
        hdpi    : 72x72 px
        xhdpi   : 96x96 px
        xxhdpi  : 144x144 px
        xxxhdpi : 192x192 px
    -->
    <icon src="resources/android/icon-app-ldpi.png" density="ldpi" />
    <icon src="resources/android/icon-app-mdpi.png" density="mdpi" />
    <icon src="resources/android/icon-app-hdpi.png" density="hdpi" />
    <icon src="resources/android/icon-app-xhdpi.png" density="xhdpi" />
    <icon src="resources/android/icon-app-xxhdpi.png" density="xxhdpi" />
    <icon src="resources/android/icon-app-xxxhdpi.png" density="xxxhdpi" />
</platform>
```
