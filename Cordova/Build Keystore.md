# Keystore for Google Play Store

```bash
keytool -genkey -alias mykey -keyalg RSA -keystore mykey.keystore -validity 3650 -keysize 2048
```

`build.json`

```json
{
    "android": {
        "debug": {
            "keystore": "/absolute/path/to/mykey.keystore",
            "storePassword": "password",
            "alias": "mykey",
            "password" : "password",
            "keystoreType": "",
            "packageType": "apk"
        },
        "release": {
            "keystore": "/absolute/path/to/mykey.keystore",
            "storePassword": "password",
            "alias": "mykey",
            "password" : "password",
            "keystoreType": "",
            "packageType": "bundle"
        }
    }
}
```

```bash
cordova build android --release --buildConfig=build.json
```