# MySQL / PHPMyAdmin

## Disable ONLY_FULL_GROUP_BY in MySQL

````mysql
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
````

----

## MySQL STRICT mode

Enable STRICT mode:

````sql
set global sql_mode = 'STRICT_TRANS_TABLES'
````

Disable STRICT mode:

````sql
set global sql_mode = '';
````

## Change the date format in MySQL

Use the following command to convert the date format of a string.

````mysql
DATE_FORMAT(date_column, '%d.%m.%Y')
````

## Remove line breaks before CSV export in MySQL

If you want to export the data from a MySQL table to a CSV file, often line breaks are making trouble. Because newlines are interpreted as the end of a row within the CSV file, we can quickly destroy the export data.

````mysql
replace(column, CHAR(13,10), '')
````

## MySQL Search & Replace String

This is a simple MySQL string to search and replace whatever you want.

````mysql
UPDATE table SET column = REPLACE(column, 'pattern', 'replace') WHERE column LIKE '%pattern%'
````

