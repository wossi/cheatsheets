# OXID

## MySQL: #1227 - Access denied; SUPER privilege(s)

If you import a database backup and you probably get the following error:

````
#1227 - Access denied; you need (at least one of) the SUPER privilege(s) for this operation
````

### Workaround

- Create you database dump without ````oxv_*```` tables or delete these tables from the current dump.
- Import your database backup.
- Login in OXID backend and go to "Service/Tools" and update the views.

_Directly after the import of the database the store is displayed as OFFLINE, this is correct because you have to update the views before as described in point 3._

Since these tables are automatically compiled and "only" serve a faster database query, they can be deleted without hesitation.