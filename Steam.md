# Steam

## Remove featured games from the Steam game collector showcase

Open the browser console and type the following command to remove featured games from the game collector showcase:

````
SetShowcaseConfig(2, 1, 0);
````

> 2 = Featured Game Showcase
> 1 = Item Position (0-3)
> 0 = Disable

## Upload long images for screenshot or artwork in Steam profile showcases

Open the browser console and enter the following lines of code for…

### Upload as an Artwork:

````
$J('#image_width').val('1000');
$J('#image_height').val('1');
````

### Upload as an Screenshot:

````
$J('#image_width').val('1000');
$J('#image_height').val('1');
$J('[name="file_type"]').val("5");
````

