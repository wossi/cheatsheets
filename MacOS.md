# MacOS

## Clear DNS cache

```
sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder
```
## Show Limits on MacOS

```bash
launchctl limit maxfiles
```
## Terminal Commands

```bash
curl https://www.domain.xyz
ping -t 30 www.domain.xyz
nslookup www.domain.xyz

dig @ns.nameserver.xyz +short MX domain.xyz
dig @ns.nameserver.xyz +short CNAME mail.domain.xyz
dig @ns.nameserver.xyz +short A mail.domain.xyz
dig @ns.nameserver.xyz +short TXT domain.xyz
```
## Get MD5 Hash of String

```bash
md5 -s joh316
```

## Reload .bash_profile

```bash
source ~/.bash_profile
```

## Switch PHP and Restart Apache Server

Use the `sphp` script from:
https://gist.github.com/rhukster/f4c04f1bf59e0b74e335ee5d186a98e2

```bash
sphp 8.3
sudo apachectl stop
sudo apachectl start
```

**Hint:** Don't use the `sudo apachectl restart` command, because there is an issue with apples pre-installed apache server and homebrew.