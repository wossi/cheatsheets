# TYPO3
## Extend Typo3 Extension
Go to "ext_localconf.php" in our template extension and add with XCLASS:
````php
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']
[\GeorgRinger\News\Controller\NewsController::class] = [
    'className' =>
\EXT\Template\Controller\NewsController::class
];
````

## Install SASS under TYPO3

Go to ````template/Resources/Public/Assets```` and create a ````Src```` folder. Open this folder in terminal and rum the following command:

````
npm init clubdrei-typo3-site-package
````

The created ````package.json```` should looks like this:

````json
{
  "name": "site-package",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "webpack-dev-server --mode=development",
    "build": "webpack --mode=production"
  },
  "author": "Name of the Author",
  "license": "",
  "devDependencies": {
    "autoprefixer": "^9.6.1",
    "copy-webpack-plugin": "^5.0.1",
    "css-hot-loader": "^1.4.4",
    "css-loader": "^2.1.1",
    "cssnano": "^4.1.10",
    "file-loader": "^3.0.1",
    "mini-css-extract-plugin": "^0.5.0",
    "node-sass": "^4.11.0",
    "postcss-loader": "^3.0.0",
    "sass-loader": "^7.1.0",
    "style-loader": "^0.23.1",
    "url-loader": "^1.1.2",
    "webpack": "^4.35.2",
    "webpack-cli": "^3.3.5",
    "webpack-dev-server": "^3.7.2"
} }
````

Copy ````postcss.config.js```` and ````webpack.config.js```` from another project.

Paste the following TypoScript in ````page.typoscript```` to overrite some settings:

````typescript
[applicationContext matches "/^Development/"]
page {
  includeCSS.headerStyles >
  includeJSFooter {
    footerScripts = https://domain.dev:37555/scripts/footer.js
    footerScripts.external = 1
    footerScripts.disableCompression = 1
    footerScripts.excludeFromConcatenation = 1
  }
}
[end]
````

After that, you can execute the following command in in ````Src```` folder:

````
npm run dev
````

## InstallTool Password

Password: joh316

````php
'installToolPassword' => 'bacb98acf97e0b6112b1d1b650b84971',
````

## Oops, an error occurred!

Add this TypoScript ````config.typoscript```` and refresh the error page.

```typescript
config.contentObjectExceptionHandler = 0
```

You can also take a look in ````typo3temp/var/logs````

## Add a new Tab on Backend Pages

You can add a new tab on pages to append custom settings or whatever...

Add your new input field to the ```ext_tables.sql```:

````sql
CREATE TABLE pages (
  tx_template_page_option  INT(11) UNSIGNED DEFAULT NULL
);
````

In your extension directory "Configuration/TCA/Overrides" add a file named ```pages.php```:

````php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addT CAcolumns('pages', [
  'tx_template_page_option' => [
    'label' => 'New Option',
    'config' => [
      'type' => 'check',
      'items' => [
        ['Enable this Option', 'true'],
      ],
    ],
  ],
]);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addT oAllTCAtypes('pages', '--div--;My Custom Settings,tx_template_page_option');
````

## Typo3 Development Setup

- Create new project in PHPStorm like "PhpstormProjects/customer/www.customer.com" (Don't create a "/public" folder!)
- Open this folder in terminal and clone GIT repository with
  ````rm -rf .idea && git clone git@project_url .````
- Run a rsync
  ````rsync -ave ssh user@domain.com:/home/user/source/ .````
- Run
  ````composer install````

### Troubleshooting

- Search in "packages/template/Configuration/TypoScript/" for ````[applicationContext = Development/*]```` and replace it with your developer domain
- Check ````.htaccess```` for compression modules and may be disable it
- Clear "typo3temp" folder (beware of ````index.html````) and flush cache in TYPO3 backend
- Check "typo3conf/PackageStates.php" if all extensions are enabled

**If you can't run tasks in PHPStorm project, try following:**

- If "node_modules" dosen't exists in extension, go to directory where the ````Gruntfile.js```` located (e.g. "packages/template/Resources/Public/ Assets/Src") and run ````npm ci````
- If ````package-lock.json```` dosen't exists, run ````npm install```` first

### Image Processing

````
[GFX][processor_path]=/Users/john/homebrew/bin/
[GFX][processor_path]=/Users/john/homebrew/bin/
````

