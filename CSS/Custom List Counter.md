# Custom Counter for Listings

```css
ol {
	counter-reset: custom-counter;
}

ol li {
	counter-increment: custom-counter;
}

ol li:before {
	content: '0' counter(custom-counter) '.';
}
```