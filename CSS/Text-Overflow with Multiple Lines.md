# Text-Overflow with Multiple Lines

```css
selector {
	display: -webkit-box;
	max-height: 128px;
	text-overflow: ellipsis;
	overflow: hidden;
	-webkit-line-clamp: 3;
	-webkit-box-orient: vertical;
}
```
