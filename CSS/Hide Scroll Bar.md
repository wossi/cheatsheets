# Hide Scroll Bar

```css
selector {
	scrollbar-width: none;
}

selector::-webkit-scrollbar {
	display: none;
}
```
