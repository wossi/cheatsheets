# Add Line-Breaks in CSS Content Property

```css
selector:before {
	content: "Lorem ipsum \A dolor sit amet";
	white-space: pre-wrap;
}
```
