# Fluid Fonts

```css
.selector {
	--vw-min: 320;
    --vw-max: 1200;
    --text-min: 12;
    --text-max: 64;
    
	font-size: calc((1px * var(--text-min)) + (var(--text-max) - var(--text-min)) * ((100vw - (1px * var(--vw-min))) / (var(--vw-max) - var(--vw-min))));
}
```
