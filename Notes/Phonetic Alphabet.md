# Phonetic Alphabet

When I want to spell a word on the phone, in most cases I use words that come to mind. All well and good … *well, maybe not*! **The Phonetic Alphabet** is actually meant for this.

So that I always have my personal spelling board or radio alphabet at hand, here is the complete list. I chose the NATO variant here because it sounds much more groovy than the normal German version. :)

| A   | Alfa     |
| --- | -------- |
| B   | Bravo    |
| C   | Charlie  |
| D   | Delta    |
| E   | Echo     |
| F   | Foxtrot  |
| G   | Golf     |
| H   | Hotel    |
| I   | India    |
| J   | Juliet   |
| K   | Kilo     |
| L   | Lima     |
| M   | Mike     |
| N   | November |
| O   | Oscar    |
| P   | Papa     |
| Q   | Quebec   |
| R   | Romeo    |
| S   | Sierra   |
| T   | Tango    |
| U   | Uniform  |
| V   | Victor   |
| W   | Whiskey  |
| X   | X-Ray    |
| Y   | Yankee   |
| Z   | Zulu     |
