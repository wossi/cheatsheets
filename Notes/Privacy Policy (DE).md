# Privacy Policy

```html
<h1>Datenschutz</h1>
<h2>Information über die Erhebung personenbezogener Daten</h2>
<p>Im Folgenden informieren wir über die Erhebung personenbezogener Daten bei Nutzung unserer Website.
        Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, z.B. Name, Adresse, E-Mail
        Adressen, Nutzerverhalten. Verantwortlicher gem. Art. 4 Abs. 7 EU-Datenschutz-Grundverordnung (DS-GVO) ist John
        Doe, Street 1a, 01234 City (siehe Impressum). Bei Ihrer Kontaktaufnahme mit uns per E-Mail oder über ein
        Kontaktformular werden die von Ihnen mitgeteilten Daten (Ihre E-Mail Adresse, ggf. Ihr Name und Ihre
        Telefonnummer) von uns gespeichert, um Ihre Fragen zu beantworten. Die in diesem Zusammenhang anfallenden Daten
        löschen wir, nachdem die Speicherung nicht mehr erforderlich ist, oder schränken die Verarbeitung ein, falls
        gesetzliche Aufbewahrungspflichten bestehen. Falls wir für einzelne Funktionen unseres Angebots auf beauftragte
        Dienstleister zurückgreifen oder Ihre Daten für werbliche Zwecke nutzen möchten, werden wir Sie untenstehend im
        Detail über die jeweiligen Vorgänge informieren. Dabei nennen wir auch die festgelegten Kriterien der
        Speicherdauer.</p>
<h2>Ihre Rechte</h2>
<p>Sie haben gegenüber uns folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:</p>
<ul>
  <li>Recht auf Auskunft,</li>
  <li>Recht auf Berichtigung oder Löschung,</li>
  <li>Recht auf Einschränkung der Verarbeitung,</li>
  <li>Recht auf Widerspruch gegen die Verarbeitung,</li>
  <li>Recht auf Datenübertragbarkeit.</li>
</ul>
<p>Sie haben zudem das Recht, sich bei einer Datenschutz-Aufsichtsbehörde über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu beschweren.</p>
<h2>Erhebung personenbezogener Daten bei Besuch unserer Website</h2>
<p>Bei der bloß informatorischen Nutzung der Website, also wenn Sie sich nicht registrieren oder uns anderweitig
        Informationen übermitteln, erheben wir nur die personenbezogenen Daten, die Ihr Browser an unseren Server
        übermittelt. Wenn Sie unsere Website betrachten möchten, erheben wir die folgenden Daten, die für uns technisch
        erforderlich sind, um Ihnen unsere Website anzuzeigen und die Stabilität und Sicherheit zu gewährleisten
        (Rechtsgrundlage ist Art. 6 Abs. | S. 1 | lit. f DS-GVO):</p>
<ul>
  <li>IP-Adresse</li>
  <li>Datum und Uhrzeit der Anfrage</li>
  <li>Zeitzonendifferenz zur Greenwich Mean Time (GMT)</li>
  <li>Inhalt der Anforderung (konkrete Seite)</li>
  <li>Zugriffsstatus / HTTP-Statuscode</li>
  <li>jeweils übertragene Datenmenge</li>
  <li>Website, von der die Anforderung kommt</li>
  <li>Browser</li>
  <li>Betriebssystem und dessen Oberfläche</li>
  <li>Sprache und Version der Browsersoftware.</li>
</ul>
<p>Zusätzlich zu den zuvor genannten Daten werden bei Ihrer Nutzung unserer Website Cookies auf Ihrem Rechner
        gespeichert. Bei Cookies handelt es sich um kleine Textdateien, die auf Ihrer Festplatte dem von Ihnen
        verwendeten Browser zugeordnet gespeichert werden und durch welche der Stelle, die den Cookie setzt (hier durch
        uns), bestimmte Informationen zufließen. Cookies können keine Programme ausführen oder Viren auf Ihren Computer
        übertragen. Sie dienen dazu, das Internetangebot insgesamt nutzerfreundlicher und effektiver zu machen.</p>
<h3>Einsatz von Cookies</h3>
<p>a) Diese Website nutzt folgende Arten von Cookies, deren Umfang und Funktionsweise im Folgenden erläutert werden:</p>
<ul>
  <li>Transiente Cookies (b)</li>
  <li>Persistente Cookies (c).</li>
</ul>
<p>b) Transiente Cookies werden automatisiert gelöscht, wenn Sie den Browser schließen. Dazu zählen insbesondere die
        Session-Cookies. Diese speichern eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen Ihres
        Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann Ihr Rechner wiedererkannt werden, wenn Sie auf
        unsere Website zurückkehren. Die Session-Cookies werden gelöscht, wenn Sie sich ausloggen oder den Browser
        schließen.</p>
<p>c) Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie
        unterscheiden kann. Sie können die Cookies in den Sicherheitseinstellungen Ihres Browsers jederzeit löschen.</p>
<p>d) Sie können Ihre Browser-Einstellungen entsprechend Ihren Wünschen konfigurieren und z.B. die Annahme von
        Third-Party-Cookies oder allen Cookies ablehnen. Wir weisen Sie darauf hin, dass Sie eventuell nicht alle
        Funktionen dieser Website nutzen können.</p>
<h2>Weitere Funktionen und Angebote unserer Website</h2>
<p>Neben der rein informatorischen Nutzung unserer Website bieten wir verschiedene Leistungen an, die Sie bei
        Interesse nutzen können. Dazu müssen Sie in der Regel weitere personenbezogene Daten angeben, die wir zur
        Erbringung der jeweiligen Leistung nutzen und für die die zuvor genannten Grundsätze zur Datenverarbeitung
        gelten.</p>
<p>Teilweise bedienen wir uns zur Verarbeitung Ihrer Daten externer Dienstleister. Diese wurden von uns sorgfältig
        ausgewählt und beauftragt, sind an unsere Weisungen gebunden und werden regelmäßig kontrolliert. Soweit unsere
        Dienstleister oder Partner ihren Sitz in einem Staat außerhalb des Europäischen Wirtschaftsraumen
        (EWR) haben, informieren wir Sie über die Folgen dieses Umstands in der Beschreibung des Angebotes.</p>
<h2>Widerspruch oder Widerruf gegen die Verarbeitung Ihrer Daten</h2>
<p>Falls Sie eine Einwilligung zur Verarbeitung Ihrer Daten erteilt haben, können Sie diese jederzeit widerrufen.
        Ein solcher Widerruf beeinflusst die Zuverlässigkeit der Verarbeitung Ihrer personenbezogenen Daten, nachdem Sie
        ihn gegenüber uns ausgesprochen haben. Soweit wir die Verarbeitung Ihrer personenbezogenen Daten auf die
        Interessenabwägung stützen, können Sie Widerspruch gegen die Verarbeitung einlegen. Dies ist der Fall, wenn die
        Verarbeitung insbesondere nicht zur Erfüllung eines Vertrags mit Ihnen erforderlich ist, was von uns jeweils bei
        der nachfolgenden Beschreibung der Funktionen dargestellt wird. Bei Ausübung eines solchen Widerspruchs bitten
        wir um Darlegung der Gründe, weshalb wir Ihre personenbezogenen Daten nicht wie von uns durchgeführt verarbeiten
        sollten. Im Falle Ihres begründeten Widerspruchs prüfen wir die Sachlage und werden entweder die
        Datenverarbeitung einstellen bzw. anpassen oder Ihnen unsere zwingenden schutzwürdigen Gründe aufzeigen,
        aufgrund derer wir die Verarbeitung fortführen.</p>
<p>Selbstverständlich können Sie der Verarbeitung Ihrer personenbezogenen Daten für Zwecke der Werbung und Datenanalyse jederzeit widersprechen. Über Ihren Werbewiderspruch können Sie uns unter folgenden Kontaktdaten informieren:</p>
<p>John Doe<br/>
  Street 1a<br/>
  01234 City<br/>
  Country<br/>
  <br/>
  Telefon: <a href="tel:+0123456789">+01234 56 789</a><br/>
  E-Mail: <a href="mailto:john.doe@domain.com">john.doe@domain.com</a></p>
```