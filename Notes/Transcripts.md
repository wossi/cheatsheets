# Volkswagen Public TV Ad - 2007

> Erst wenn ein Auto eine Ära geprägt hat.
> Wenn es zu einer Lebenseinstellung wurde.
> Und einer ganzen Generation, ihren Namen gab.
> 
> Erst wenn ein Auto nicht nur groß ist, sondern Größe hat.
> Wenn es ganzen Familien ein Zuhause ist, und jeden an seinen Innovationen teilhaben lässt.
> 
> Wenn es sich mit keinem Rekord zufrieden gibt und Technologien schafft, die neue Wege ermöglichen.
> Erst wenn ein Auto heute schon an morgen denkt, dann ist es...
> 
> Das Auto.