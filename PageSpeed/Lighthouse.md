# Lighthouse (PageSpeed)
## Defer Loading Background Images

Add the following CSS and JS into `<head>` section.

````html
<style id="defer-style">
  selector {
    background-image: none !important;
  }
</style>

<script>
  document.addEventListener('DOMContentLoaded', function () {
    const deferStyle = document.getElementById('defer-style');

    if (null !== deferStyle) {
      deferStyle.parentElement.removeChild(deferStyle);
    }
  });
</script>
````

## Preload Assets

````html
<link rel="preload" href="/path/to/font.woff2" as="font" type="font/woff2" crossorigin/>
<link rel="preload" href="/path/to/vector.svg" as="image" type="image/svg+xml"/>
<link rel="preload" fetchpriority="high" href="/path/to/image.png" as="image" type="image/webp"/>
````
