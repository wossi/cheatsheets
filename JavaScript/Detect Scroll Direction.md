# Detect Scroll Direction

```javascript
let originScrollPosition = window.scrollY;

document.addEventListener('scroll', function() {
	var currentScrollPosition = window.scrollY;

	if (currentScrollPosition < originScrollPosition) {
		console.log('up');
	} else {
		console.log('down');
	}
  
	originScrollPosition = currentScrollPosition;
});
```