# Browser History Back Button

```javascript
(function () {  
    const historyBackButtons = document.querySelectorAll('.selector');  
  
    if (0 === historyBackButtons.length) {  
        return;  
    }  
  
    historyBackButtons.forEach((historyBackButton) => {  
        historyBackButton.addEventListener('click', (event) => {  
            event.preventDefault();  
            window.history.back();  
        });  
    });  
})();
```