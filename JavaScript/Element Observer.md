# Element Observer

```js
(function () {
    const elements = document.querySelectorAll('.selector');
    const observer = new IntersectionObserver(entries => {
        entries.forEach((entry) => {
            entry.target.classList.toggle('.selector--visible', entry.isIntersecting);
        });
    });

    elements.forEach((element) => {
        observer.observe(element);
    });
})();
```