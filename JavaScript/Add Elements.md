# Add Elements

```javascript
const container = document.querySelector('.selector');
const element = document.createElement('div');

// before
container.insertAdjacentElement('beforebegin', element);

// inside
container.appendChild(element);

// after
container.insertAdjacentElement('afterend', element);
```
