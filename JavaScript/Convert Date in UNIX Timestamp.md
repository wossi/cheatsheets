# Convert Date in UNIX Timestamp

You can convert a date like "1970-01-01" in a UNIX timestamp with this simple code snippet.

```javascript
const date = new Date('1970-01-01');
const timestamp = Math.floor(date.getTime() / 1000);

console.log(timestamp);
```