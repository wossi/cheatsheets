```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3D Object Viewer</title>
    <style>
        body,
        html {
            display: block;
            width: 100%;
            height: 100%;
            position: relative;
            margin: 0;
            font-family: monospace;
            font-weight: normal;
            font-size: 10px;
            line-height: 1.8;
        }

        canvas {
            display: block;
        }

        #loader {
            display: block;
            width: 16px;
            height: 16px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            border: 2px solid #efefef;
            border-top: 2px solid #0971de;
            border-radius: 100%;
            animation: spin 512ms linear infinite;
        }

        @keyframes spin {
            0% {
                transform: translate(-50%, -50%) rotate(0deg);
            }

            100% {
                transform: translate(-50%, -50%) rotate(360deg);
            }
        }
    </style>
</head>
<body>

<div id="loader"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r128/three.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/three@0.128.0/examples/js/controls/OrbitControls.js"></script>
<script src="https://cdn.jsdelivr.net/npm/three@0.128.0/examples/js/loaders/OBJLoader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/three@0.128.0/examples/js/loaders/MTLLoader.js"></script>

<script>
    const file = 'filename';

    let scene, camera, renderer, controls;

    function init() {
        // Szene erstellen
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0xffffff);

        // Kamera erstellen
        camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 5000);
        camera.position.z = 5;

        // Renderer erstellen
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);

        // OrbitControls hinzufügen
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true; // Dämpfung für sanftere Bewegungen
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false; // Kein Schwenken in der Bildschirmebene

        // Licht hinzufügen
        const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
        scene.add(ambientLight);

        const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
        directionalLight.position.set(1, 1, 1).normalize();
        scene.add(directionalLight);

        // MTL-Loader verwenden, um Materialien zu laden
        const mtlLoader = new THREE.MTLLoader();
        mtlLoader.setPath('./'); // Pfad zur MTL-Datei
        mtlLoader.load(`${file}.mtl`, (materials) => {
            materials.preload();

            // OBJ-Loader verwenden, um das 3D-Modell zu laden
            const objLoader = new THREE.OBJLoader();
            objLoader.setMaterials(materials);
            objLoader.setPath('./'); // Pfad zur OBJ-Datei

            // Objekt laden
            objLoader.load(
                `${file}.obj`,
                (object) => {
                    // Objekt erfolgreich geladen
                    scene.add(object);

                    // Objekt zentrieren und Kamera anpassen (wie zuvor)
                    const box = new THREE.Box3().setFromObject(object);
                    const size = box.getSize(new THREE.Vector3()).length();
                    const center = box.getCenter(new THREE.Vector3());

                    object.position.x += (object.position.x - center.x);
                    object.position.y += (object.position.y - center.y);
                    object.position.z += (object.position.z - center.z);

                    const halfSize = size * 0.5;
                    const fov = camera.fov * (Math.PI / 180);
                    const distance = Math.abs(halfSize / Math.sin(fov / 2));

                    camera.position.set(0, 0, distance);
                    camera.lookAt(center);

                    controls.update(); // OrbitControls aktualisieren

                    // Spinner entfernen
                    document.getElementById('loader').style.display = 'none';
                },
                // Progress-Callback (optional, um den Fortschritt zu zeigen)
                (xhr) => {
                    const percentComplete = (xhr.loaded / xhr.total) * 100;
                    console.log(`${Math.round(percentComplete, 2)}% loaded`);
                },
                // Fehler-Callback
                (error) => {
                    console.error('Error while loading object:', error);
                }
            );
        });

        // Fenstergröße anpassen
        window.addEventListener('resize', onWindowResize, false);
    }

    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function animate() {
        requestAnimationFrame(animate);

        // Dämpfung für die OrbitControls anwenden
        controls.update();

        renderer.render(scene, camera);
    }

    init();
    animate();
</script>
</body>
</html>
```
