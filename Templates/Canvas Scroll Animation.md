```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
    <title>Scroll Animation</title>
    <style>
        html,
        body {
            display: block;
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            background-color: #000;
        }
 
        body {
            height: 200vh;
        }
 
        canvas {
            display: block;
            width: 100%;
            height: 100%;
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>
<canvas></canvas>
<script>
    const html = document.documentElement;
 
    const win = {
        w: window.innerWidth,
        h: window.innerHeight,
    }
 
    const canvas = document.querySelector('canvas');
    const context = canvas.getContext('2d');
 
    let frameCount = 80;
    frameCount = frameCount - 1;
    const currentFrame = index => (
        `./assets/img/video-frame-${index.toString().padStart(3, '0')}.jpg`
    )
 
    const preloadImages = () => {
        for (let i = 0; i < frameCount; i++) {
            const image = new Image();
            image.src = currentFrame(i);
        }
    };
 
    const image = new Image();
 
    image.src = currentFrame(1);
 
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
 
    const drawImage = (image, type = 'cover') => {
        const imageRatio = image.height / image.width
        const winRatio = window.innerHeight / window.innerWidth
 
        if ((imageRatio < winRatio && type === 'contain') || (imageRatio > winRatio && type === 'cover')) {
            const h = window.innerWidth * imageRatio
            context.drawImage(image, 0, (window.innerHeight - h) / 2, window.innerWidth, h)
        }
 
        if ((imageRatio > winRatio && type === 'contain') || (imageRatio < winRatio && type === 'cover')) {
            const w = window.innerWidth * winRatio / imageRatio
            context.drawImage(image, (win.w - w) / 2, 0, w, window.innerHeight)
        }
    }
 
    image.onload = function () {
        drawImage(image);
    }
 
    const updateImage = index => {
        image.src = currentFrame(index);
        drawImage(image);
    }
 
    const resizeCanvas = () => {
        win.w = window.innerWidth
        win.h = window.innerHeight
        canvas.width = win.w
        canvas.height = win.h
        canvas.style.width = `${win.w}px`
        canvas.style.height = `${win.h}px`
    }
 
    const init = () => {
        resizeCanvas();
        drawImage(image);
    }
 
    window.addEventListener('scroll', () => {
        const scrollTop = html.scrollTop;
        const maxScrollTop = html.scrollHeight - window.innerHeight;
        const scrollFraction = scrollTop / maxScrollTop;
        const frameIndex = Math.min(
            frameCount - 1,
            Math.ceil(scrollFraction * frameCount)
        );
 
        requestAnimationFrame(() => updateImage(frameIndex + 1));
    });
 
    window.addEventListener('resize', init);
 
    preloadImages();
</script>
</body>
</html>
```
