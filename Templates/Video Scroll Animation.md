```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
    <title>Video Scroll Animation</title>
    <style>
        html,
        body {
            display: block;
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            background-color: #000;
        }

		body {
			height: 500vh;
		}
 
        video {
            display: block;
            width: 100%;
            height: 100%;
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            object-fit: cover;
        }
    </style>
</head>
<body>
<video src="./assets/video.mp4" preload muted loop playsinline></video>
<script>
    /**
     * FFMPEG Converter Online: https://ffmpeg-online.vercel.app/
     * -vf scale=1280:-1 -movflags faststart -vcodec libx264 -crf 18 -g 2 -pix_fmt yuv420p -an
     *
     * ffmpeg -i input.mp4   # input file name
     *   -vf scale=1280:-1   # width of output movie file in pixels
     *   -movflags faststart # it never hurts
     *   -vcodec libx264     # h.264 encoder
     *   -crf 18             # quality setting, 18 = excellent
     *   -g 2                # keyframe every x frames
     *   -pix_fmt yuv420p    # pixel format
     *   -an                 # no audio (you won't hear it anyway)
     *   output.mp4          # outfile file name
     */
    const html = document.documentElement;
    const video = document.querySelector('video');
 
    // handle on scroll event
    window.addEventListener('scroll', () => {
        const scrollTop = html.scrollTop;
        const maxScrollTop = html.scrollHeight - window.innerHeight;
 
        let scrollFraction = scrollTop / maxScrollTop;
 
        if (video.duration > 0) {
            video.currentTime = video.duration * scrollFraction;
        }
    });
</script>
</body>
</html>
```