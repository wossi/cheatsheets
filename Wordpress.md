# WordPress

## My WordPress Plugins

I work a lot professionally and also in my free time with WordPress. I have known this CMS since the release of version 2.3 in 2007. In the meantime, I cannot imagine a better system for myself and my customers. It can be easily and quickly expanded using plugins. If you have the certain technical know-how, this opens up unlimited possibilities … I love it and have already been able to implement a number of projects.

I am always asked which plugins I use or would use myself. The function and when which plugin is used, I just now assume. So here are *my personal recommendations*:

- [Elementor](https://wordpress.org/plugins/elementor/) (Pro) by Elementor.com
- [Advanced Custom Fields](https://wordpress.org/plugins/advanced-custom-fields/) (Pro) by Elliot Condon
- [SVG Support](https://wordpress.org/plugins/svg-support/) by Benbodhi
- [Code Snippets](https://wordpress.org/plugins/code-snippets/) by Code Snippets Pro
- [Redirection](https://wordpress.org/plugins/redirection/) by John Godley
- [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) by Team Yoast
- [Fast Velocity Minify](https://wordpress.org/plugins/fast-velocity-minify/) by Raul Peixoto
- [GDPR Cookie Consent](https://wordpress.org/plugins/cookie-law-info/) by WebToffee
- [Advanced Access Manager](https://wordpress.org/plugins/advanced-access-manager/) by Vasyl Martyniuk

## Custom Domain for WordPress Pages

If you want to use a domain for a WordPress page, add the following snippet to your `.htaccess` before the WordPress section.

```
RewriteEngine On
RewriteBase /
RewriteCond %{HTTP_HOST} domain\.com$ [NC]
RewriteRule ^(.*)$ index.php?page_id=1879 [NC,QSA,L]

# BEGIN WordPress
...
```

## Change Field Name in Advanced Custom Fields

You have created a field in Advanced Custom Fields Plugin and have already saved data in it. You want to rename the field and your data is no longer visible in the backend? No problem. Log into PHPMyAdmin and run the following commands.

````
UPDATE wp_postmeta SET meta_key = 'new_key' WHERE meta_key = 'old_key'
````

## Add and Remove CSS for WordPress Dashboard

### Add Inline Style

````php+HTML
add_action('admin_head', function () {
  ?>
  <style>
    .wp-block {
      max-width: none;
    }
  </style>
  <?php
});
````

### Add Stylesheet File

````php
add_action('admin_enqueue_scripts', function () {
  wp_enqueue_style('handle', get_template_directory_uri() . '/style.css');
});
````

### Remove Style Handles via Loop

````php
public function dequeueStyles() {
    $handles = [
        'astra-theme-css',
        'astra-addon-css',
        'elementor-global',
    ];

    foreach($handles as $handle) {
        wp_dequeue_style($handle);
    }
}
````

## Add CCS Classes to body

### Frontend

````php
add_filter('body_class', function ($classes) {
  $classes[] = 'custom-class';
  return $classes;
});
````

### Backend (Dashboard)

````php
add_filter('admin_body_class', function ($classes) {
  $classes .= ' custom-class';
  return $classes;
});
````

## Insert HTML after Opening `<body>`-Tag

This only works with WordPress 5.2.0+

```php
add_action('wp_body_open', function () {
	// insert html
});
```

## Change the WordPress administrator user ID

Once you have done a WordPress database backup, connect to your WordPress database using the MySQL command line tool or the web based phpMyAdmin and execute the below queries on the WordPress database.

````mysql
UPDATE wp_users SET ID = 1024 WHERE ID = 1;
UPDATE wp_usermeta SET user_id = 1024 WHERE user_id = 1;
````

This MySQL query will change the default WordPress administrator user ID from 1 to 1024 in the wp_users table and wp_usermeta table, where user related data is stored.

````mysql
ALTER TABLE wp_users AUTO_INCREMENT = 2048;
````

Once you execute the above query, WordPress will assign a user ID of 2049 to the next WordPress user you create.

> **Note:** Always specify a high value for the new WordPress administrator ID. The higher the value is the less chances of it being discovered and the longer an attack will take.

## Customize WordPress Login Errors

Type the following code snippet in your `functions.php`

````php
add_filter('login_errors', function () {
	return __('Access Denied!');
}, 99);
````

## Limit WordPress Login Attempts

````php
add_filter('authenticate', function ($user, $username, $password) {
	if (get_transient('attempted_login')) {
		$datas = get_transient('attempted_login');

		if ($datas['tried'] >= 3) {
			$until = get_option('_transient_timeout_' . 'attempted_login');
			$time = time_to_go($until);

			return new WP_Error('too_many_tried', sprintf(__('<strong>ERROR</strong>: You have reached authentication limit, you will be able to try again in %1$s.'), $time));
		}
	}

	return $user;
}, 30, 3);

add_action('wp_login_failed', function ($username) {
	if (get_transient('attempted_login')) {
		$datas = get_transient('attempted_login');
		$datas['tried']++;

		if ($datas['tried'] <= 3) {
      set_transient('attempted_login', $datas, 300);
    }
	} else {
		$datas = [
			'tried' => 1
		];
		
    set_transient('attempted_login', $datas, 300);
	}
}, 10, 1);

function time_to_go($timestamp) {
  $periods = [
    'second',
    'minute',
    'hour',
    'day',
    'week',
    'month',
    'year',
	];
  
	$lengths = [
    '60',
    '60',
    '24',
    '7',
    '4.35',
    '12',
	];

  $current_timestamp = time();
  $difference = abs($current_timestamp - $timestamp);
  
	for ($i = 0; $difference >= $lengths[$i] && $i < count($lengths) - 1; $i++) {
		$difference /= $lengths[$i];
	}
  
	$difference = round($difference);
  
	if (isset($difference)) {
		if ($difference != 1) {
      $periods[$i] .= 's';
    }
      
		return $difference . ' ' . $periods[$i];
  }
}
````

## Move jQuery to Footer for Google Page Speed Insights

Beware on render-blocking resources of jQuery. You can move jQuery in WordPress footer to solve this problem.

````php
add_action('wp_enqueue_scripts', function () {
  if (false === is_user_logged_in()) {
    wp_scripts()->add_data('jquery', 'group', 1);
    wp_scripts()->add_data('jquery-core', 'group', 1);
    wp_scripts()->add_data('jquery-migrate', 'group', 1);
  }
}, 99);
````

## Delete Broken Media Files from WordPress Database

Run the following lines in phpMyAdmin. The files in `wp-content/uploads` are not affected.

````mysql
DELETE FROM wp_posts WHERE post_type = 'attachment';
DELETE FROM wp_postmeta WHERE meta_key = '_wp_attached_file';
DELETE FROM wp_postmeta WHERE meta_key = '_wp_attachment_metadata';
````

## Show Future Posts in Frontend

````php
add_action('pre_get_posts', function ($query) {
  if (false === is_user_logged_in() && $query->is_main_query() && $query->is_single()) {
    $query->set('post_status', [
      'publish',
      'future'
    ]);
  }
});
````

## Disable WordPress Updates

Add this to your `wp-config.php`

````php
define('WP_AUTO_UPDATE_CORE', false);
````

## Overwrite Stylesheets in WordPress

Use these methods to overwrite existing stylesheets and you no longer have to use !important for your sections.

Header Scripts:

````php
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('new', 'css/new.css', ['old'], false);
}, 99);
````

Footer Scripts:

````php
add_action('get_footer', function () {
    wp_enqueue_style('new', 'css/new.css', ['old'], false);
}, 99);
````

## Ultimate WordPress .htaccess File

```
# BEGIN SSL Forwarding
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteCond %{HTTPS} !=on
	RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>
# END SSL Forwarding

# BEGIN WordPress
<IfModule mod_rewrite.c>
 RewriteEngine On
 RewriteBase /
 RewriteRule ^index\.php$ - [L]
 RewriteCond %{REQUEST_FILENAME} !-f
 RewriteCond %{REQUEST_FILENAME} !-d
 RewriteRule . /index.php [L]
</IfModule>
# END WordPress

# BEGIN CORS Security
<IfModule mod_headers.c>
	<FilesMatch "\.(ttf|ttc|otf|eot|woff|woff2|font.css|css|js|gif|png|jpe?g|svg|svgz|ico|webp)$">
		Header set Access-Control-Allow-Origin "*"
	</FilesMatch>
</IfModule>
# END CORS Security

# BEGIN Browser Caching
<IfModule mod_expires.c>
	ExpiresActive on
	ExpiresDefault "access plus 1 month"

	# CSS
	ExpiresByType text/css "access plus 1 year"

	# Data
	ExpiresByType application/atom+xml "access plus 1 hour"
	ExpiresByType application/rdf+xml "access plus 1 hour"
	ExpiresByType application/rss+xml "access plus 1 hour"

	ExpiresByType application/json "access plus 0 seconds"
	ExpiresByType application/ld+json "access plus 0 seconds"
	ExpiresByType application/schema+json "access plus 0 seconds"
	ExpiresByType application/vnd.geo+json "access plus 0 seconds"
	ExpiresByType application/xml "access plus 0 seconds"
	ExpiresByType text/xml "access plus 0 seconds"

	# Favicon (cannot be renamed!) and cursor images
	ExpiresByType image/vnd.microsoft.icon "access plus 1 week"
	ExpiresByType image/x-icon "access plus 1 week"

	# HTML
	ExpiresByType text/html "access plus 0 seconds"

	# JavaScript
	ExpiresByType application/javascript "access plus 1 year"
	ExpiresByType application/x-javascript "access plus 1 year"
	ExpiresByType text/javascript "access plus 1 year"

	# Manifest files
	ExpiresByType application/manifest+json "access plus 1 week"
	ExpiresByType application/x-web-app-manifest+json "access plus 0 seconds"
	ExpiresByType text/cache-manifest "access plus 0 seconds"

	# Media files
	ExpiresByType audio/ogg "access plus 1 year"
	ExpiresByType image/bmp "access plus 1 year"
	ExpiresByType image/gif "access plus 1 year"
	ExpiresByType image/jpeg "access plus 1 year"
	ExpiresByType image/png "access plus 1 year"
	ExpiresByType image/svg+xml "access plus 1 year"
	ExpiresByType image/webp "access plus 1 year"
	ExpiresByType video/mp4 "access plus 1 year"
	ExpiresByType video/ogg "access plus 1 year"
	ExpiresByType video/webm "access plus 1 year"

	# Web Fonts

	# Embedded OpenType (EOT)
	ExpiresByType application/vnd.ms-fontobject "access plus 1 year"
	ExpiresByType font/eot "access plus 1 year"

	# OpenType
	ExpiresByType font/opentype "access plus 1 year"

	# TrueType
	ExpiresByType application/x-font-ttf "access plus 1 year"

	# Web Open Font Format (WOFF) 1.0
	ExpiresByType application/font-woff "access plus 1 year"
	ExpiresByType application/x-font-woff "access plus 1 year"
	ExpiresByType font/woff "access plus 1 year"

	# Web Open Font Format (WOFF) 2.0
	ExpiresByType application/font-woff2 "access plus 1 year"

	# Other
	ExpiresByType text/x-cross-domain-policy "access plus 1 week"
</IfModule>
# END Browser Caching

# BEGIN Compression
<IfModule mod_deflate.c>
	AddOutputFilterByType DEFLATE text/plain
	AddOutputFilterByType DEFLATE text/html
	AddOutputFilterByType DEFLATE text/xml
	AddOutputFilterByType DEFLATE text/css
	AddOutputFilterByType DEFLATE text/vtt
	AddOutputFilterByType DEFLATE text/x-component
	AddOutputFilterByType DEFLATE application/xml
	AddOutputFilterByType DEFLATE application/xhtml+xml
	AddOutputFilterByType DEFLATE application/rss+xml
	AddOutputFilterByType DEFLATE application/js
	AddOutputFilterByType DEFLATE application/javascript
	AddOutputFilterByType DEFLATE application/x-javascript
	AddOutputFilterByType DEFLATE application/x-httpd-php
	AddOutputFilterByType DEFLATE application/x-httpd-fastphp
	AddOutputFilterByType DEFLATE application/atom+xml
	AddOutputFilterByType DEFLATE application/json
	AddOutputFilterByType DEFLATE application/ld+json
	AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
	AddOutputFilterByType DEFLATE application/x-font-ttf
	AddOutputFilterByType DEFLATE application/font-woff2
	AddOutputFilterByType DEFLATE application/x-font-woff
	AddOutputFilterByType DEFLATE application/x-web-app-manifest+json font/woff
  AddOutputFilterByType DEFLATE font/woff
  AddOutputFilterByType DEFLATE font/opentype
  AddOutputFilterByType DEFLATE image/svg+xml
	AddOutputFilterByType DEFLATE image/x-icon

	# Exception: Images
	SetEnvIfNoCase REQUEST_URI \.(?:gif|jpg|jpeg|png|svg)$ no-gzip dont-vary

	# Drop problematic browsers
	BrowserMatch ^Mozilla/4 gzip-only-text/html
	BrowserMatch ^Mozilla/4\.0[678] no-gzip
	BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html

	# Make sure proxies don't deliver the wrong content
	Header append Vary User-Agent env=!dont-vary
</IfModule>
# END Compression

# BEGIN Caching on Apache
<IfModule mod_headers.c>
	<FilesMatch "\.(ico|pdf|flv|swf|js|css|gif|png|jpg|jpeg|txt|woff2|woff)$">
		Header set Cache-Control "max-age=31536000, public"
	</FilesMatch>
</IfModule>

<IfModule mod_headers.c>
	<FilesMatch "\.(js|css|xml|gz)$">
		Header append Vary Accept-Encoding
	</FilesMatch>
</IfModule>
# END Caching on Apache

# BEGIN Keep-Alive Header
<IfModule mod_headers.c>
	Header set Connection keep-alive
</IfModule>
# END Keep-Alive Header

# BEGIN Disable ETags
<IfModule mod_expires.c>
	<IfModule mod_headers.c>
		Header unset ETag
	</IfModule>

	FileETag None
</IfModule>

<IfModule mod_headers.c>
	<FilesMatch ".(js|css|xml|gz|html|woff|woff2|ttf)$">
		Header append Vary: Accept-Encoding
	</FilesMatch>
</IfModule>
# END Disable ETags

# BEIGN Protection
<Files install.php>
	Order allow,deny
	Deny from all
</Files>

<Files wp-config.php>
	Order allow,deny
	Deny from all
</Files>

<Files readme.html>
	Order allow,deny
	Deny from all
	Satisfy all
</Files>

<Files liesmich.html>
	Order allow,deny
	Deny from all
	Satisfy all
</Files>

<Files error_log>
	Order allow,deny
	Deny from all
</Files>

<FilesMatch "(\.htaccess|\.htpasswd)">
	Order deny,allow
	Deny from all
</FilesMatch>

<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /
	RewriteRule ^wp-admin/includes/ - [F,L]
	RewriteRule !^wp-includes/ - [S=3]
	RewriteRule ^wp-includes/[^/]+\.php$ - [F,L]
	RewriteRule ^wp-includes/js/tinymce/langs/.+\.php - [F,L]
	RewriteRule ^wp-includes/theme-compat/ - [F,L]
</IfModule>

<Files xmlrpc.php>
	Order deny,allow
	Deny from all
</Files>
# END Protection
```
### Optional Snippets

```
# BEGIN No-Referrer Header
<IfModule mod_headers.c>
	Header set Referrer-Policy "no-referrer"
</IfModule>
# END No-Referrer Header

# BEGIN X-Frame-Options Header
<IfModule mod_headers.c>
	Header set X-Frame-Options "sameorigin"
</IfModule>
# END X-Frame-Options Header

# BEGIN X-XSS-Protection Header
<IfModule mod_headers.c>
	Header set X-XSS-Protection "1; mode=block"
</IfModule>
# END X-XSS-Protection Header

# BEGIN X-Content-Type-Options Header
<IfModule mod_headers.c>
	Header set X-Content-Type-Options "nosniff"
</IfModule>
# END X-Content-Type-Options Header

# BEGIN Strict-Transport-Security Header
<IfModule mod_headers.c>
	Header set Strict-Transport-Security "max-age=31536000; includeSubDomains; preload"
</IfModule>
# END Strict-Transport-Security Header
```
### Restrict Login Access

**Note:** Don't use comments in the following sections!

```
<Files wp-login.php>
    Order deny,allow
    Deny from all
    Allow from 127.0.0.1
</Files>
```
#### Restrict Login Access Behind Reverse Proxy

```
<Files wp-login.php>
    SetEnvIF X-FORWARDED-FOR "127.0.0.1" MyIP
    Order deny,allow
    Deny from all
    Allow from env=MyIP
</Files>
```

## WordPress robots.txt

```
User-Agent: *

Disallow: /wp-content/plugins/
Disallow: /wp-admin/

Allow: /wp-content/uploads/
```

## WordPress Rest API

First we need to add the following code to our `functions.php`.

````php
add_action('rest_api_init', function () {
	register_rest_route('namespace', '/path/(?P<id>\d+)', [
		'methods' => 'GET',
		'callback' => function ($data) {
			return $data['id']; // or $_GET for parameters
		}
	]);
});
````

Then we can create a simple form and process the request via Javascript.

````javascript
$('form').on('submit', function (e) {
	e.preventDefault();

	$.ajax({
		type: 'GET',
		url: '/wp-json/namespace/path/99',
		data: $('form').serialize()
	}).done(function (response) {
		console.log(response);
	});
});
````

Alternatively, you can also call the following URL directly in the browser:
https://www.domain.com/wp-json/namespace/path?parameter=value

## Allow WordPress Editors to Edit Menus

It often happens that customers want to do their own thing in the dashboard (or backend) of Wordpress. In the course of this, it may be necessary to enable the editor to edit menus. All you have to do is add the following lines to your `functions.php`.

````php
$role_object = get_role('editor');
$role_object->add_cap('edit_theme_options');
````

## Allow WordPress Contributors to Upload Media Files

````php
$role_object = get_role('contributor');
$role_object->add_cap('upload_files');
````

## Allow WordPress Contributors to Save Custom Settings

If you add custom settings in WordPress, a Contributor maybe must save these settings. You must give them access to mange options.

````php
$role_object = get_role('contributor');
$role_object->add_cap('manage_options');
````

## Allow Editors to Edit Privacy Page

```php
add_action('map_meta_cap', function ($caps, $cap, $user_id, $args) {  
    $user_meta = get_userdata($user_id);  
  
    if (!$user_meta) {  
        return $caps;  
    }  
  
    if ('manage_privacy_options' !== $cap) {  
        return $caps;  
    }  
  
    if (!array_intersect(['editor', 'administrator'], $user_meta->roles)) {  
        return $caps;  
    }  
  
    $manage_name = is_multisite() ? 'manage_network' : 'manage_options';  
    $caps = array_diff($caps, [$manage_name]);  
  
    return $caps;  
}, 1, 4);
```

## Get WordPress Menu Items as Array

````php
function getNavigationItemsArray(array $menuItems)
{
	$navigationItems = [];
	$parentGroups = [];

	foreach ($menuItems as $menuItem) {
		if ($menuItem->menu_item_parent == 0) {
			continue;
		}

		if (!isset($parentGroups[$menuItem->menu_item_parent])) {
			$parentGroups[$menuItem->menu_item_parent] = [$menuItem];
		} else {
			$parentGroups[$menuItem->menu_item_parent][] = $menuItem;
		}
	}

	foreach ($menuItems as $menuItem) {
		if (isset($parentGroups[$menuItem->ID])) {
			$menuItem->children = $parentGroups[$menuItem->ID];
		} else {
			$menuItem->children = [];
		}

		if ($menuItem->menu_item_parent == 0) {
			$navigationItems[] = $menuItem;
		}
	}

	return $navigationItems;
}
````

## Rename WordPress Database after Domain Transfer

It happens that you have to transfer a WordPress project into the live environment after development. After the move you are faced with the problem that the old URL is still in the database. The project is therefore not accessible via the new domain. However, it is relatively easy to replace the old domain with the new domain. To do this, you have to execute the following SQL commands in PHPMyAdmin.

````mysql
UPDATE wp_options SET option_value = replace(option_value, 'http://www.old.com', 'http://www.new.com') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET guid = replace(guid, 'http://www.old.com', 'http://www.new.com');
UPDATE wp_posts SET post_content = replace(post_content, 'http://www.old.com', 'http://www.new.com');
UPDATE wp_postmeta SET meta_value = replace(meta_value, 'http://www.old.com', 'http://www.new.com');
````

> The names of the tables should of course be adjusted accordingly.

## Use ACF Fields in Yoast SEO as Template Variable

If you want to use the content of a user-defined field of the ACF plugin (Advanced Custom Fields) in the Yoast SEO Plugin, you can easily pass the value to Yoast as a template variable. It is therefore possible to use the content of the field in the SEO snippet as a title or meta description.

````php
add_action('wpseo_register_extra_replacements', function () {
	wpseo_register_var_replacement('%%custom_template_variable%%', function () {
		global $post;

		$field = get_field_object('acf_field_name', $post->ID);
		return $field['value']->name;
	}, 'advanced', __('ACF Field Description'));
});
````

### Show custom field in SEO snippet

Then you can use the template variable `%%custom_template_variable%%` to edit the SEO snippet in Yoast plugin.

## WordPress Post Query

Show posts in Wordpress … probably the best option!

````php
global $post;

$query = [
	'post_type' => 'post',
	'post_status' => 'publish',
	'orderby' => 'date',
	'order' => 'DESC',
	'posts_per_page' => -1,
	'cat' => 1,
  # 'suppress_filters' => false,
];

$posts = get_posts($query);

if (!empty($posts)) {
	foreach($posts as $post) {
    setup_postdata($post);
    
    the_title();
		the_content('', false);
  }
}

wp_reset_postdata();
````

### Filter by Taxonomy

````php
$query['tax_query'] = [
	[
		'taxonomy' => 'my_custom_taxonomy',
		'field' => 'term_id',
		'terms' => 1
	]
];
````

### Filter by Custom Field

````php
$query['meta_query'] = [
	[
		'key' => 'my_custom_field',
		'compare' => '=', // LIKE (string), IN (array), NOT EXISTS (string) etc.
		'value' => '"1"', // string with "" or array
	],
];
````

#### Trouble-Shooting after adding new ACF Field (True/False)

If you add a new ACF field, e.g. a switch (True/False) that controls the visibility of a post type, you must first check whether this field exists.

```php
$query['meta_query'] = [
	'relation' => 'OR',
	[
		'key' => 'my_custom_field',
		'compare' => 'NOT EXISTS',
	],
	[
		'key' => 'my_custom_field',
		'compare' => '!=', // LIKE (string), IN (array), NOT EXISTS (string) etc.
		'value' => '1', // string with "" or array
	],
];
```

## WordPress Login without Database Access

Ok, the following scenario:

> We received a WordPress project from a customer. The customer does not have administrator access to the backend. The former developer of the project is no longer available and the login to the database is not known. The only access is the connection to FTP server.

### How can you still get full access to Wordpress Backend?

We only need to add the following lines to the `functions.php` in the activated theme:

````php
add_action('init', function (){
	$user = 'USERNAME';
	$pass = 'PASSWORD';
	$mail = 'USER@MAIL.COM';

	if (false === username_exists($user)  && false === email_exists($mail)) {
		$user_id = wp_create_user($user, $pass, $mail);
		$user = new WP_User($user_id);
		$user->set_role('administrator');
	}
});
````

After that, the website just has to be called up. The corresponding user is automatically created in the database. Don't forget to delete these lines of code after the process has finished.

## Add CSS Class to Pages Table Row in WordPress Dashboard

````php
add_action('load-edit.php', function () {
    add_filter('post_class', function ($classes) {
        if (1 === get_the_ID()) {
            $classes[] = 'custom-class';
        }

        return $classes;
    });
});
````

## Custom Post Link

You can link a post with an external source instead of default single post of WordPress.

````php
add_filter('post_link', function ($link, $post) {
	$meta = get_post_meta($post->ID, 'custom_post_link', true);
  $url = esc_url(filter_var($meta, FILTER_VALIDATE_URL));
  
  return $url ? $url : $link;
), 10, 2);
````

## Limit Search of WordPress

You can setup the onpage search of WordPress, to search only in posts:

````php
add_filter('pre_get_posts', function ($query) {
  if ($query->is_search) {
  	$query->set('post_type', 'post');
  }
});
````

## Remove Inline-Style of Tag Cloud Link

````php
add_filter('wp_generate_tag_cloud', function ($tag_string) {
  return preg_replace('/style="font-size:.+pt;"/', '', $tag_string);
}, 10, 3);
````

## Translate WordPress Strings

````php
add_filter('gettext', function ($translation, $text, $domain) {
  if ('domain' === $domain) {
    switch ($translation) {
      case 'string to translate':
        $translation = __('my custom translation', 'domain');
        break;
    }
  }
  
  return $translation;
}, 30, 3);
````

## Local Configuration for Development Environment

````php
if (isset($_SERVER['WP_CONTEXT']) && 'Development' === $_SERVER['WP_CONTEXT']) {
  // Development
	define('DB_NAME', 'development');
	define('DB_USER', 'local');
	define('DB_PASSWORD', '0000');
  
  define('WP_HOME', 'http://localhost');
  define('WP_SITEURL', 'http://localhost');
} else {
  // Production
  define('DB_NAME', 'production');
	define('DB_USER', 'live');
	define('DB_PASSWORD', '9999');
}
````

```
SetEnv WP_CONTEXT Development
Options Indexes FollowSymLinks
```

## Add Dashboard Notices

````php
add_action('admin_notices', function () {
  ?>
  <div class="notice notice-warning is-dismissible">
    <p><?= __('This is a dashboard notice.', 'domain'); ?></p>
  </div>
  <?php
});
````

## Disable Plugin Updates and Code Editor

````php
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', true);
````

## WP Statistics behind Reverse Proxy

Put these lines of code to top in your `wp-config.php`

````php
if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $list = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
    $_SERVER['REMOTE_ADDR'] = $list[0];
}
````

## Remove the "Private" Prefix from Title

````php
add_filter('the_title', function ($title) {
    return str_replace('Private: ', '', $title);
});
````

## Increase WordPress Memory

````php
define('WP_MEMORY_LIMIT', '512M');
````

## Redirect Public Visitors to Front Page

````php
add_action('template_redirect', function () {
    if (\Elementor\Plugin::$instance->preview->is_preview_mode()) {
        return;
    }

    if (!is_home() && !is_front_page()) {
        if (false === is_user_logged_in()) {
            wp_safe_redirect(get_bloginfo('home'));
        }
    }

    if (is_home() || is_front_page()) {
        if (false !== is_user_logged_in()) {
            wp_safe_redirect(get_permalink(get_page_by_path('subpage')));
        }
    }
}, 1);
````

## Allow Custom Filetypes to Media Upload

```php
add_filter('upload_mimes', function ($mimes) {
  $mimes['dwg'] = 'application/acad';
  $mimes['dxf'] = 'application/dxf';
  $mimes['svg'] = 'image/svg+xml';

  return $mimes;
});
```

## Prevend User Deletion by ID

````php
add_action('delete_user', function($id) {
    if (43 === $id) {
        die('Please not delete this account!');
    }
});
````

## WP CLI

Install WordPress CLI with the following command:

```bash
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --info
```

### Remove all Thumbnails from Media Library

```bash
cd /wp-content/uploads
find . -iname '*-*x*.jpg' | xargs ls
find . -iname '*-*x*.jpg' | xargs rm -f
```

## Enqueue Style/Script with Shortcode Dependency

```php
global $post;

if (has_shortcode($post->post_content, 'shortcode')) {
	wp_enqueue_style('style', 'style.css', [], null);
}

if (has_shortcode($post->post_content, 'shortcode')) {
  wp_enqueue_script('script', 'script.js', [], null, true);
}
```

## Remove Admin Bar for all Users

```php
add_action('after_setup_theme', function () {
	show_admin_bar(false);
});
```

## Add Custom Image Sizes

```php
add_image_size('custom-size-128', 128, 128);
add_image_size('custom-size-256', 256, 156);
add_image_size('custom-size-512', 512, 512);
```

## Disable "scaled"-Version for Uploads

```php
add_filter('big_image_size_threshold', '__return_false');
```

## Change Image Quality

```php
add_filter('jpeg_quality', function () {
	return 100; // Default: 85
});
```

## Wrapper for Embed Objects

```php
add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
    return '<div class="embed-wrapper">' . $html . '</div>';
}, 10, 4);
```

## Change Language Attribute

```php
add_filter('language_attributes', function () {
  return 'lang="en-US"';
});
```

## Add Column in Dashboard for Post Thumbnails

```php
add_filter('manage_posts_columns', function ($columns) {
    $key = 'post_thumbnail';
    $value = __('Thumbnail', 'domain');
    $position = 2;
 
	return array_slice($columns, 0, $position, true) + [$key => $value] + array_slice($columns, $position, count($columns) - 1, true);
});

add_action('manage_posts_custom_column', function ($column_name, $post_ID) {
    if ($column_name == 'post_thumbnail') {
        echo get_the_post_thumbnail($post_ID, [100, 100]);
    }
}, 10, 2);
```

## Custom WPML Language Switch

```php
add_shortcode('custom_language_switch', function () {
  global $sitepress;

  $class = [
    'de' => [
      'language-switch__option',
    ],
    'en' => [
      'language-switch__option',
    ],
  ];

  $class[ICL_LANGUAGE_CODE][] = 'language-switch__option--active';

  ob_start();

  ?>
  <div class="language-switch">
    <div class="language-switch__options">
      <div class="<?= implode(' ', $class['de']); ?>">
        <a href="<?= $sitepress->convert_url(get_the_permalink(), 'de'); ?>" class="language-switch__option-link"><?= __('DE', 'domain'); ?></a>
      </div>
      <div class="<?= implode(' ', $class['en']); ?>">
        <a href="<?= $sitepress->convert_url(get_the_permalink(), 'en'); ?>" class="language-switch__option-link"><?= __('EN', 'domain'); ?></a>
      </div>
    </div>
  </div>
  <?php

  return ob_get_clean();
});
```

## Custom Logfile for Wordpress Debugging

```php
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', '/error.log'); // absolute path
define('WP_DEBUG_DISPLAY', false);
```
## Enable/Disable Comments and Pings for Posts with SQL

```sql
# disable
UPDATE wp_posts SET comment_status = 'closed', ping_status = 'closed'
# enable
UPDATE wp_posts SET comment_status = 'open', ping_status = 'open'
```
## Change Table Prefix

First, you'll need to rename the `$table_prefix` in `wp-config.php`. After that, you can change the table names with operations in phpMyAdmin. Finally you need to run the following SQL command:

```sql
update NEW_usermeta set meta_key = 'NEW_capabilities' where meta_key = 'OLD_capabilities';
update NEW_usermeta set meta_key = 'NEW_user_level' where meta_key = 'OLD_user_level';
update NEW_usermeta set meta_key = 'NEW_autosave_draft_ids' where meta_key = 'OLD_autosave_draft_ids';
update NEW_options set option_name = 'NEW_user_roles' where option_name = 'OLD_user_roles';
```

## Prepare Advanced Custom Fields (ACF Plugin)

Sometimes it can be helpful, that some custom fields (created with the ACF plugin) should be disabled or readonly for some reasons.

```php
add_filter('acf/prepare_field', function ($field) {  
    $readonlyFields = [  
        'latitude',  
        'longitude',  
    ];
    
    if (false !== in_array($field['_name'], $readonlyFields)) {  
        $field['readonly'] = true;  
    }
	
    return $field;  
});
```

...or via direct call...

```php
add_filter('acf/prepare_field/name=custom_field', function ($field) {
    if (empty($field['value'])) {
        $field['value'] = 'PREDEFINED VALUE';
    }
 
    return $field;
});
```

## Generate Post Title with ACF

```php
add_action('acf/save_post', function ($post_id) {
    if (!empty(get_the_title($post_id))) {
        return;
    }
 
    $postdata = [
        'ID' => $post_id,
        'post_title' => sprintf(__('#%s', 'domain'), $post_id),
    ];
 
    wp_update_post($postdata);
}, 10, 3);
```

## Rename Post Labels in Dashboard

```php
add_filter('post_type_labels_post', function ($args) {
    foreach ($args as $key => $label) {
        if (null === $label) {
            continue;
        }
 
        $args->{$key} = str_replace([__('Posts'), __('Post')], [__('Stories', 'domain'), __('Story', 'domain')], $label);
    }
 
    return $args;
});
```

## Custom Post Order in Dashboard

```php
add_action('pre_get_posts', function ($query) {
    if (isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'appointment') {
        $query->set('orderby', 'meta_value');
        $query->set('meta_key', 'custom_field');
        $query->set('order', 'DESC');
    }
 
    return $query;
});
```