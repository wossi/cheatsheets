# Cordova App Development for Android

### Use Google AdMob in Cordova App

Install AdMob Plus Cordova Plugin:

```bash
cordova plugin add admob-plus-cordova --save --variable APP_ID_ANDROID=ca-app-pub-xxx~xxx
```

 `config.xml`
 
```xml
<?xml version='1.0' encoding='utf-8'?>  
<widget id="com.app.demo" version="1.0.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">  
    <name>My App</name>  
    <content src="index.html" />  
    <access origin="*" />  
    <allow-intent href="https://*/*" />  
    <platform name="android">  
        <preference name="AdMobPlusWebViewAd" value="true" />  
        <preference name="android-targetSdkVersion" value="33" />  
        <preference name="AndroidXEnabled" value="true" />  
        <preference name="GradlePluginKotlinEnabled" value="true" />
        <allow-intent href="market:*" />  
    </platform>
    <engine name="android" spec="9.1.0" />  
</widget>
```

`admob.js`

```javascript
'use strict';  
  
let lastAdTime = 0;  
  
const app = {  
    initialize() {  
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);  
  
        document.addEventListener('admob.ad.load', (evt) => {  
            const {ad} = evt;  
            console.log('admob.ad.load', ad.id, ad instanceof admob.AppOpenAd);  
        }, false);  
  
        document.addEventListener('admob.ad.dismiss', (evt) => {  
            console.log('admob.ad.dismiss', evt.ad.id, lastAdTime);  
        }, false);  
  
        document.addEventListener('admob.ad.show', (evt) => {  
            console.log('admob.ad.show', Object.keys(evt));  
        }, false);  
    },  
    onDeviceReady() {  
        admob.start().catch(alert);  
  
        this.showBannerAd();  
        this.initButton('rewarded-ad', this.showRewardedAd);  
    },  
    showBannerAd() {  
        const banner = new admob.BannerAd({  
            adUnitId: 'ca-app-pub-3940256099942544/6300978111',  
            position: 'top',  
            // offset: 0,  
        });  
  
        return banner.show();  
    },  
    showRewardedAd() {  
        const rewarded = new admob.RewardedAd({  
            adUnitId: 'ca-app-pub-3940256099942544/5224354917',  
        });  
  
        rewarded.on('dismiss', () => {  
            lastAdTime = Date.now();  
        });  
  
        return rewarded.load().then(() => rewarded.show());  
    },  
    initButton(id, displayAd) {  
        const button = document.getElementById(id);  
  
        if (null === button) {  
            return;  
        }  
  
        button.addEventListener('click', function () {  
            button.disabled = true;  
  
            displayAd().catch(alert).then(function () {  
                button.disabled = false;  
            });  
        });  
    },  
}  
  
app.initialize();
```