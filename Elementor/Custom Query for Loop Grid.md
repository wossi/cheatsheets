# Custom Query for Loop Grid

```php
add_action('elementor/query/my_custom_query', function ($query) {
    $metaQuery = $query->get('meta_query');
 
    if (!$metaQuery) {
        $metaQuery = [];
    }

    $metaQuery[] = [
        'key' => '_thumbnail_id',
    ];

    $query->set('meta_query', $metaQuery);

	// OR

	$query->set('orderby', 'date');
	$query->set('order', 'ASC')
});
```