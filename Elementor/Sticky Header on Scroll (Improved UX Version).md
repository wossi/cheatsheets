# Sticky Header on Scroll (Improved UX Version)

```javascript
(function () {  
    const elementorLocationHeader = document.querySelector('.elementor-location-header');  
  
    if (null === elementorLocationHeader) {  
        return;  
    }  
  
    const documentBody = document.querySelector('body');  
    const scrollEffectOffset = elementorLocationHeader.offsetHeight;  
  
    let lastScrollTop = 0;  
  
    ['DOMContentLoaded', 'scroll'].forEach((eventListener) => {  
        document.addEventListener(eventListener, () => {  
            const currentScrollTop = window.scrollY;  
  
            if (currentScrollTop >= scrollEffectOffset && currentScrollTop > lastScrollTop) {  
                // scroll down  
                documentBody.classList.add('elementor-location-header--sticky');  
            } else {  
                // scroll up  
                documentBody.classList.remove('elementor-location-header--sticky');  
            }  
  
            lastScrollTop = currentScrollTop;  
        });  
    });  
})();
```
