# Web Development

## Blank HTML5 Template (Boilerplate)

index.html

````html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Default Page Title</title>
	<link rel="icon" href="favicon.png">
	<link href="assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<script src="assets/js/script.js"></script>
</body>
</html>
````

style.css
````css
body,
html {
  display: block;
  width: 100%;
  height: 100%;
  position: relative;
  margin: 0;
  padding: 0;
  background-color: #000;
  font-family: sans-serif;
  font-weight: normal;
  font-size: 14px;
  color: #fff;
  line-height: 1;
}

*,
*:before,
*:after {
    font-family: inherit;
    user-select: none;
    box-sizing: border-box;
}
````

script.js
````javascript
(function () {
  
})();
````



## Clear DNS Cache in Google Chrome Browser

Sometimes it is helpful to clear the DNS cache in the browser. In Google Chrome you just have to open a new tab, enter the following in the address bar and confirm with *ENTER*. Then simply click on the button and empty the DNS cache.

````
chrome://net-internals/#dns
````

## Detect a Development Environment

````php
if (isset($_SERVER['WP_CONTEXT']) && 'Development/Name' === $_SERVER['WP_CONTEXT']) {
  // Development
} else {
  // Production
}
````

To set the environment variable add this to your `.htaccess` file.

````
Options Indexes FollowSymLinks
SetEnv WP_CONTEXT Development/Name
````

## Colored Browser for Google Chrome Mobile

````html
<meta name="theme-color" content="#f00">
````
## 100% Viewport Height on Mobile Devices

````javascript
(function () {
    function setViewportHeight() {
        const vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', vh + 'px');
    }

    setViewportHeight();
    window.addEventListener('resize', function () {
        setViewportHeight();
    });
});
````

Do not forget to define a fallback in CSS...

````css
selector {
	height: 100vh;
	height: calc(var(--vh, 1vh) * 100);
}
````
## Zoom-In Animation... One by One

Ok, put that Javascript snippet on your site.

````javascript
$(window).on('scroll', function () {
	var scrollPosition = $(window).scrollTop() + $(window).height();
	var timeout = 100;

	$('.item').each(function () {
		var element = $(this);
		var elementPosition = element.offset().top;

		if (false === element.hasClass('item--animate') && scrollPosition >= elementPosition) {
			setTimeout(function () {
				element.addClass('item--animate');
			}, timeout += 100);
		}
	});
});
````

The code above will add a class on your elements, if the user scroll down on your site and the element is in the viewport.
Don't forget to style the elements, to make the effect more fancy.

````css
.post {
	transform: scale(0);
	visibility: hidden;
	opacity: 0;
  transition: transform 300ms ease, visibility 0ms, opacity 300ms ease;
}

.post.post--animate {
	transform: scale(1);
	visibility: visible;
	opacity: 1;
}
````

## CSS Hamburger Menu

A really nice Hamburg menu button. Simple and yet so brilliant.
First the basic framework with HTML:

````html
<button class="navigation-button">
	<span class="navigation-button__hamburger">
		<span class="navigation-button__line"></span>
		<span class="navigation-button__line"></span>
		<span class="navigation-button__line"></span>
	</span>
	<span class="navigation-button__cross">
		<span class="navigation-button__line"></span>
		<span class="navigation-button__line"></span>
	</span> 
</button>
````

… and then a little bit of CSS:

````css
selector {
	z-index: 99;
}
    
.navigation-button {
	display: block;
	width: 40px;
	height: 40px;
	position: relative;
	border: none;
	padding: 0;
	background: none;
	cursor: pointer;
  outline: 0;
}
    
.navigation-button__hamburger,
.navigation-button__cross {
	display: block;
	width: 60%;
	height: 60%;
	position: absolute;
	top: 20%;
	left: 20%;
}
    
.navigation-button__line {
	display: block;
	width: 100%;
	height: 2px;
	position: absolute;
	background-color: #000;
}
    
.navigation-button__hamburger .navigation-button__line {
	right: 0;
	transform: translateY(-50%);
	transition: width 0.15s ease-in-out;
}
    
.navigation-button__hamburger .navigation-button__line:nth-child(1) {
	top: calc(50% - 4px);
	transition-delay: 0.4s;
}
    
.navigation-button__hamburger .navigation-button__line:nth-child(2) {
  width: 75%;
	top: 50%;
	transition-delay: 0.5s;
}
    
.navigation-button__hamburger .navigation-button__line:nth-child(3) {
	top: calc(50% + 4px);
	transition-delay: 0.6s;
}
    
.navigation-button--clicked .navigation-button__hamburger .navigation-button__line {
	width: 0;
}
    
.navigation-button--clicked .navigation-button__hamburger .navigation-button__line:nth-child(1) {
	transition-delay: 0.1s;
}
    
.navigation-button--clicked .navigation-button__hamburger .navigation-button__line:nth-child(2) {
	transition-delay: 0.2s;
}
    
.navigation-button--clicked .navigation-button__hamburger .navigation-button__line:nth-child(3) {
	transition-delay: 0.3s;
}
    
.navigation-button__cross {
	transform: rotate(45deg);
}
    
.navigation-button__cross .navigation-button__line:nth-child(1) {
	width: 0;
	top: 50%;
	left: 0;
	transform: translateY(-50%);
	transition: width 0.15s ease-in-out;
	transition-delay: 0.1s;
}
    
.navigation-button__cross .navigation-button__line:nth-child(2) {
	width: 2px;
	height: 0;
	top: 0;
	left: 50%;
	transform: translateX(-50%);
	transition: height 0.15s ease-in-out;
	transition-delay: 0.2s;
}
    
.navigation-button--clicked .navigation-button__cross .navigation-button__line:nth-child(1) {
	width: 100%;
	transition-delay: 0.5s;
}
    
.navigation-button--clicked .navigation-button__cross .navigation-button__line:nth-child(2) {
	height: 100%;
	transition-delay: 0.6s;
}
````

Now we can simply swap classes with the OnClick event via jQuery and marvel at the magic. Mmh, yummi. :)

## iOS Hover Fix / Double-Tap Problem

iPad and iPhone sometimes have the so-called "Double tap" problem. The problem occurs together with the CSS selector ": hover". The iOS interprets the mouseover effect, which actually does not exist on mobile devices. There is a simple solution: just copy the following code into the `<head>` area of the page. Alternatively, the two snippets can also be stored in separate files.

````javascript
document.addEventListener('touchstart', function() {}, true);
````

````css
*:hover,
*:active {
	-webkit-user-select: none;
	-webkit-touch-callout: none;
}
````

## CSS Selector Hacks

### Internet Explorer 11

If the properties of a CSS selector should only be valid for Internet Explorer 11, then we use the following instruction...

````css
_:-ms-fullscreen,
:root selector {
  property: value;
}
````

### Microsoft Edge Browser

If the properties of a CSS selector should only be valid for the Microsoft Edge browser, then we use the following instruction...

````css
:-ms-lang(x),
:-webkit-full-screen,
selector {
	property: value;
}
````

## Responsive Images

Default style for images...

````css
img {
	display: block;
	width: 100%;
	height: auto;
	margin: auto;
}
````

The HTML part...

````html
<picture>
	<source media="(min-width: 1025px)" srcset="image-full.jpg">
	<source media="(min-width: 768px) and (max-width: 1024px)" srcset="image-large.jpg">
	<source media="(min-width: 481px) and (max-width: 767px)" srcset="image-medium.jpg">
	<source media="(max-width: 480px)" srcset="image-small.jpg">
	<img src="image-full.jpg">                   
</picture>
````

## base64 Scanlines

Sometimes you can use scanlines quite well … but in most cases you would have to use a graphics program to create the things. Fortunately, this can also be done with a few lines of code.

### Black Scanlines

````css
/* 1 Pixel Version */
background-image: url('data:image/gif;base64,R0lGODlhAQACAPAAAAAAAAAAACH5BAEAAAEALAAAAAABAAIAAAICRAoAOw==');
/* 2 Pixel Version */
background-image: url('data:image/gif;base64,R0lGODlhAQAEAPAAAAAAAAAAACH5BAEAAAEALAAAAAABAAQAAAIDBBIFADs=');
````

### White Scanlines

````css
/* 1 Pixel Version */
background-image: url('data:image/gif;base64,R0lGODlhAQACAPAAAP///wAAACH5BAEAAAEALAAAAAABAAIAAAICRAoAOw==');
/* 2 Pixel Version */
background-image: url('data:image/gif;base64,R0lGODlhAQAEAPAAAP///wAAACH5BAEAAAEALAAAAAABAAQAAAIDBBIFADs=');
````

Don't forget to repeat the background horizontally!

## 1 Pixel Transparent GIF

Somehow I always used a transparent pixel... no idea why?! Before you start your CorelDraw (or Photoshop), here is a solution in base64 format.

````html
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
````

## Custom Cursor with CSS

Sometimes a simple mouse pointer (cursor) is not enough. Then something of your own has to be found. Replacing the cursors with another graphic is in many cases insufficient or leads to compatibility problems in other browsers. We need something better!

First of all we need some HTML code (place before ``</body>``):

````html
<div class="cursor">
	<div class="cursor-pointer"></div>
	<div class="cursor-effect"></div>
</div>
````

Then we give the elements their essential attributes and design them according to our ideas:

````css
.cursor--ready,
.cursor--ready *,
.cursor--ready *:hover {
	cursor: none;
}

.cursor {
	display: block;
	box-sizing: border-box;
	width: 100vw;
	height: 100vh;
	position: fixed;
	top: 0;
	left: 0;
	z-index: 99999;
	pointer-events: none;
}

.cursor-pointer {
	position: absolute;
	top: -100%;
	left: -100%;
}

.cursor-pointer:before {
	content: '';
	display: block;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 4px;
	height: 4px;
	border-radius: 100%;
	background-color: #a3ff12;
	opacity: 1;
	transition: all 256ms ease;
}

.cursor--active .cursor-pointer:before {
	width: 16px;
	height: 16px;
	box-shadow: 0 0 16px #a3ff12;
	opacity: 0.64;
}

.cursor-effect {
	position: absolute;
	top: -100%;
	left: -100%;
	transition: top 100ms ease, left 100ms ease;
}

.cursor-effect:before {
	content: '';
	display: block;
	width: 24px;
	height: 24px;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	border: 1px solid rgba(163, 255, 18, 0.48);
	border-radius: 100%;
	opacity: 1;
	transition: all 256ms ease;
}

.cursor--active .cursor-effect:before {
	width: 0;
	height: 0;
	opacity: 0;
}
	
@media (max-width: 767px) {
	.cursor {
		display: none;
	}
}
````

So that everything works properly, a little bit of Javascript is missing at the end:

````javascript
(function($) {
	var cursorPointer = $('.cursor-pointer');
	var cursorEffect = $('.cursor-effect');
	var cursorEffectOffsetTop = (cursorEffect.height() / 2) - (cursorPointer.height() / 2);
	var cursorEffectOffsetLeft = (cursorEffect.width() / 2) - (cursorPointer.width() / 2);
			
	$('body').addClass('cursor--ready');
			
	$(document).on('mousemove', function (e) {
		cursorPointer.css({
			'top': e.pageY + 'px',
			'left': e.pageX + 'px'
		});
				
		cursorEffect.css({
			'top': (e.pageY - cursorEffectOffsetTop) + 'px',
			'left': (e.pageX - cursorEffectOffsetLeft) + 'px'
		});
	});
			
	$(document).on('mouseenter', 'a, label, input, button', function () {
		$('.cursor').addClass('cursor--active');
	});
			
	$(document).on('mouseleave', 'a, label, input, button', function () {
		$('.cursor').removeClass('cursor--active');
	});
})(jQuery);
````

## Facebook Pixel Track Order

Add the following snippet on confirmation page of order to track successfully purchase.

````javascript
fbq('trackCustom', 'OrderConfirmation', {
  value: 99.99,
  currency: 'USD',
  items: 'product'
});
````

## SVG Drawing

````html
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 220 220">
    <defs>
        <style>
            path {
                stroke-dasharray: 1000;
                stroke-dashoffset: 1000;
                animation: draw 2500ms ease forwards 500ms;
            }

            @keyframes draw {
                100% {
                    stroke-dashoffset: 0;
                }
            }
        </style>
    </defs>
    <path d="M181.4,72.2c-0.7-0.9-1.5-1.9-2.1-2.7c-6.6-8.5-9.9-17.9-12.9-26.3c-2.1-5.9-3.9-11.1-6.6-14.7c-7.2-10-34.4-14.1-40.3-13.9c-6.4,0.2-9.1,5.7-9.9,10.1c-1.9,0.5-4.5,1.7-6.7,4c-5.7,6,3.3,20.1,11.2,27.8c6.3,6.1,10.7,5.8,14.5,5.6c1.7-0.1,3.4-0.2,5.4,0.2c4.7,1.1,9.3,4.8,11.4,6.6c-1.8,32,3.5,47.7,5.8,54.6c-1.8-0.2-4.3-0.6-6.8-1.2c-13.8-25.5-42.3-22.2-51-20.5C81.1,82.7,59.9,76.4,44,76.9c-14.8,0.5-24.2-0.5-24.3-0.5c-1.6-0.2-3.1,0.8-3.6,2.3c-0.5,1.4-11.6,34.7,2,67.3c13.2,31.6,28.8,57.5,29,57.8c0.6,1.1,1.8,1.6,2.9,1.6c0.6,0,1.2-0.2,1.7-0.5c15.4-9.2,22-21.4,24.5-27.5c30.7,12,63.6,7.7,86.2,1.7c25.2-6.6,42.7-16.3,43.5-16.7c0.9-0.5,1.6-1.5,1.7-2.5C213.7,113.2,193.5,87.5,181.4,72.2z M201,157.3c-10.4,5.4-72.9,35.3-125.5,12.5c-0.9-0.4-2-0.4-2.9,0.1c-0.9,0.4-1.6,1.3-1.8,2.2c0,0.1-3.8,14.3-19.8,25.1c-4.4-7.6-16.4-28.9-26.7-53.9c-10.5-25.2-4.8-51.8-2.5-60c3.9,0.3,11.8,0.6,22.4,0.3c13-0.4,29.8,4.3,40.8,18.1l8.6,16.8c0.6,1.2,1.8,1.9,3,1.9c0.5,0,1.1-0.1,1.6-0.4c1.7-0.9,2.3-2.9,1.5-4.6l-3.7-7.3c8.6-1.5,32.2-3.3,43.1,18.6c0.4,0.9,1.3,1.5,2.2,1.8c13.6,3.4,15.6,1.2,16.5,0.1c1.7-1.9,1-4.1-0.1-7.4c-2.2-6.5-7.4-21.8-5.4-53.6c0.1-1-0.3-1.9-1-2.6c-0.3-0.3-7.5-7.6-15.8-9.5c-3-0.7-5.4-0.5-7.4-0.4c-3.3,0.2-5.2,0.3-9.3-3.7c-7.4-7.2-11.5-16.2-10.9-18.3c2.1-2.1,4.7-2.3,4.8-2.3c1.9,0,3.3-1.5,3.4-3.4c0-0.1,0.2-6.1,3.5-6.2c0.2,0,0.3,0,0.5,0c7.6,0,29.5,4.8,34.1,11.1c2.1,2.9,3.7,7.6,5.7,13c3,8.4,6.7,18.8,13.9,28.2c0.7,0.9,1.4,1.8,2.2,2.8C188,91.7,206.1,114.6,201,157.3z"/>
</svg>
````

## Remove Google Font Roboto from Google Maps

````javascript
var head = document.getElementsByTagName('head')[0];
var insertBefore = head.insertBefore;

head.insertBefore = function(newElement, referenceElement) {
  if (newElement.href && newElement.href.indexOf('https://fonts.googleapis.com/css?family=Roboto') === 0) {
    return;
  }

  insertBefore.call(head, newElement, referenceElement);
};
````

## Convert SVG Polygon to Path

````svg
<polygon points="966.6,86.4 923.9,86.4 923.9,63.7 1035.4,63.7 1035.4,86.4 992.9,86.4 992.9,215.2 966.6,215.2"/>
<path d="M966.6,86.4 923.9,86.4 923.9,63.7 1035.4,63.7 1035.4,86.4 992.9,86.4 992.9,215.2 966.6,215.2z"/>
````

## SVG Polygon with Background Image

```xml
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 500">
	<defs>
		<pattern id="image" width="1" height="1" preserveAspectRatio="xMidYMax slice" viewBox="0 0 2000 1333">
			<image xlink:href="https://www.domain.xyz/image.jpg" x="0" y="0" width="2000" height="1333"/>
		</pattern>
	</defs>
	<polygon fill="url(#image)" points="750, 500 0, 500 0, 0 1000, 0"/>
</svg>
```

## iOS Safari Overflow: Hidden and Border-Radius

```css
selector {
	position: relative;
	z-index: 0; /* Set a "z-index" for Safari browsers! */
	border-radius: 24px;
	overflow: hidden;
}
```