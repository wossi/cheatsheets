# Linux / Ubuntu

## Install HAL on Ubuntu

````
sudo add-apt-repository ppa:mjblenner/ppa-hal
sudo apt-get update
sudo apt-get install hal
````

## XAMPP - Install ionCube Loader on Ubuntu Localhost

Download the *.tgz files from http://www.ioncube.com/loaders.php. Open the terminal (Strg + Alt + T) and input the following command:

````
tar -zxvf loader-wizard.tgz
````

Open ionCube Loader Wizard http://127.0.0.1/ioncube/loader-wizard.php

> **Note:** If you can’t copy the *.so files to directory, open the terminal and execute:
> `gksudo nautilus`

## Install Pipelight on Ubuntu (Silverlight for Linux)

Open the Terminal and type the following commands (line by line) to install Pipelight on Ubuntu:

````
sudo add-apt-repository ppa:pipelight/stable
sudo apt-get update
sudo apt-get install pipelight
sudo pipelight-plugin --update
sudo pipelight-plugin --enable silverlight
````

## Repair GRUB2, if Ubuntu dosen't Boot

After booting into the live Ubuntu environment, open a terminal from the Dash and run the following commands to install Boot Repair:

````
sudo add-apt-repository ppa:yannubuntu/boot-repair
sudo apt-get update
sudo apt-get install -y boot-repair
boot-repair
````

The Boot Repair window will appear after you run the boot-repair command. After it scans your system, click the Recommended repair button to repair GRUB2 with a single click.

## Ubuntu System Fix - Read Error on Boot

Boot your computer while holding the Shift key. If you see a menu with a list of operating systems appear, you’ve accessed the GRUB boot loader. Select menu option "dpkg" and press Enter to use it. It will repairs broken software packages. If a package failed to install properly and your system doesn’t work because of it, this may help. Restart the system. Done.

## XAMPP - Stopping Apache fail apachectl returned 1

When you stop Xampp in your linux system and you may get an error, XAMPP Stopping Apache fail apachectl returned 1.

````
Stopping XAMPP for Linux 1.8.3-5…
XAMPP: Stopping Apache…fail.
apachectl returned 1.
XAMPP: Stopping MySQL…ok.
XAMPP: Stopping ProFTPD…kill: usage: kill [-s sigspec | -n signum | -sigspec] pid | jobspec … or kill -l [sigspec]
fail.
kill returned 1.
````

…delete the following files:

````
sudo rm /opt/lampp/logs/httpd.pid
sudo rm /opt/lampp/var/mysql/$(hostname).pid
sudo rm /opt/lampp/var/proftpd.pid
````

> **Note:** Replace "$(hostname)" with your device name (for example: localhost or whatever)