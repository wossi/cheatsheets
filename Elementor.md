# Elementor / Elementor Pro

## No Post in Elementor Posts Widget

Add this snippet to your functions.php to show a message if no posts found in Elementor Posts Widget.

````php
add_action('elementor/query/query_results', function ($query) {
  if (0 === $query->found_posts) {
    _e('No posts found.', 'domain');
  }
});
````

## Customize Flatpickr in Elementor

You can change datepicker fields in Elementor form with the following script. Keep in mind, that the script depend on `flatpickr.js`. You need no jQuery! You also need to activate the option of "Native HTML5" for the date fields in Elementor editor.

````javascript
(function () {
    document.addEventListener('DOMContentLoaded', () => {
        prepareFlatpickr();
    });

    function prepareFlatpickr() {
        function waitForFlatpickr(callback) {
            if (typeof window.flatpickr !== 'function') {
                setTimeout(function () {
                    waitForFlatpickr(callback);
                }, 100);
            }

            callback();
        }

        waitForFlatpickr(function () {
            if (typeof flatpickr === 'undefined') {
                return;
            }

            flatpickr.l10ns.de = {
                weekdays: {
                    shorthand: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                    longhand: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag']
                },
                months: {
                    shorthand: ['Jan', 'Feb', 'März', 'Apr', 'Mai', 'Juni', 'Juli', 'Aug', 'Sept', 'Okt', 'Nov', 'Dez'],
                    longhand: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
                },
                rangeSeparator: ' bis '
            };

            flatpickr.localize(flatpickr.l10ns.de);
            flatpickr.l10ns.default.firstDayOfWeek = 1;
            flatpickr('.flatpickr-input');

            setTimeout(function () {
                document.querySelectorAll('input[name="form_fields[date_from]"]').forEach((e) => {
                    e.flatpickr({
                        dateFormat: 'd.m.Y',
                        minDate: 'today',
                        onChange: function (selectedDates, dateString, instance) {
                            document.querySelector('input[name="form_fields[date_to]"]').flatpickr({
                                dateFormat: 'd.m.Y',
                                minDate: dateString,
                            });
                        }
                    });
                })

                document.querySelectorAll('input[name="form_fields[date_to]"]').forEach((e) => {
                    e.flatpickr({
                        dateFormat: 'd.m.Y',
                        minDate: 'today',
                    });
                });
            }, 100);
        });
    }
})();
````

### Troubleshooting

If you have a Elementor form in popup, you must wait until the popup was open:

````javascript
$(document).on('elementor/popup/show', function (event, id, instance) {
  if (99 === id) {
  	prepareFlatpickr();
  }
});
````

If the first day of week is incorrect, you can try so set:

````javascript
flatpickr.l10ns.default.firstDayofWeek = 1;
````

## Hide Elementor Switch Mode in Gutenberg Editor

````css
.elementor-editor-active .edit-post-layout #elementor-switch-mode {
	display: none !important;
}
````

## Remove Google Fonts from Elementor

You can dequeue Google Fonts on Elementor and link fonts with parameter `display=swap`.

````php
add_action('wp_print_styles', function () {
  wp_dequeue_style('google-fonts-1');
});
````

## Fix Autoscroll on Elementor

If you use a sticky header with anchor links, there is one problem since Elementor 3.0 was released. Everything is fine, if you have a single one page layout. But when you have subpages and use your anchor links also in the main navigation, then you will have some trouble.

You're on a subpage and want to jump to an anchor link on another page. You scroll automatically to the anchor, but now the sticky header is placed over it.

Copy and paste the following snippet and the problem will solved.

```php
add_action('wp_footer', function () {
	?>
	<script>
		var hash = window.location.hash.replace('#', '');
		var scrollToElement = document.getElementById(hash);
    var header = document.querySelector('[data-elementor-type="header"]');
    var headerHeight = header.offsetHeight;

		if (null !== scrollToElement) {
			var style = document.createElement('style');
			style.innerText = '#' + hash + ':before { content: ""; display:block; height: ' + headerHeight + 'px; margin: ' + (-1 * headerHeight) + 'px 0 0; visibility: hidden; pointer-events: none; }';
			document.head.appendChild(style);
			document.addEventListener('DOMContentLoaded', function() {
				setTimeout(function () {
					document.head.removeChild(style);
				}, 1000);
      });
    }
	</script>
	<?php
}, 1);
```

## Elementor Font Awesome 5 Pro Support (Self-Hosted)

Actually, this variant is already *obsolete*, if not already *deprecated*. However, I use this method in some projects. If e.g. [Font Awesome 5 Pro](https://fontawesome.com/pro) has already been purchased and there is no access to the CDN. With the integration of the u.g. Code snippets in the `functions.php`, there is still the possibility to use the premium icons in Elementor.

> In Elementor, the Font Awesome 5 CDN can only be used with the license key.

````php
function enqueue_icon_font() {
	wp_enqueue_style('font-awesome-5', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');
}

add_action('wp_enqueue_scripts', 'enqueue_icon_font', 1);
add_action('elementor/editor/before_enqueue_styles', 'enqueue_icon_font', 1);
add_action('elementor/controls/controls_registered', function ($controls_registry) {
	$icons = $controls_registry->get_control('icon')->get_settings('options');
	$new_icons = array_merge(
		[
      'fab fa-elementor' => 'elementor',
    ],
		$icons
	);

	$controls_registry->get_control('icon')->set_settings('options', $new_icons);
}, 10, 1);
````

## Elementor Pro Manual License Activation

[https://www.domain.com/wp-admin/admin.php?page=elementor-license&mode=manually](https://www.domain.com/wp-admin/admin.php?page=elementor-license&mode=manually)

## Handle Popups with Custom Elements

You can build a custom element, like a button or something...

````html
<button class="navigation-button">
    <span class="navigation-button__icon">
        <span class="navigation-button__icon-line"></span>
        <span class="navigation-button__icon-line"></span>
        <span class="navigation-button__icon-line"></span>
    </span>
</button>
````

...and using these lines of JavaScript to handle popups in Elementor.

```javascript
(function () {
    const elementorPopup = 99;
  
    const navigationButton = document.querySelector('.navigation-button');
  	const navigationButtonActiveClass = 'navigation-button--clicked';

    navigationButton.addEventlistener('click', (e) => {
        if (false === e.target.classList.contains(navigationButtonActiveClass)) {
            elementorProFrontend.modules.popup.showPopup({
                id: elementorPopup
            });
          	navigationButton.classList.add(navigationButtonActiveClass);
        } else {
            elementorFrontend.documentsManager.documents[elementorPopup].getModal().hide();
          	navigationButton.classList.remove(navigationButtonActiveClass);
        }
    });
  
  	window.addEventListener('elementor/popup/show', (e) => {
      if (e.detail.id === parseInt(elementorPopup)) {
        console.log(e.detail.id);
      }
    });
})();
```

### Trouble-Shooting with On-Scroll Event

If you got an error in the developer console like this:

> ReferenceError: elementorProFrontend is not defined

...you must wrap the script with this:

```javascript
document.addEventListener('DOMContentLoaded', () => {
	document.addEventListener('scroll', () => {
		// use elementorProFrontend here
	});
});
```

## [DEPRECATED] Disable Popup by Date

For example, you have an saisonal popup on your page that should no more shown after 01.01.1970, then you can use the following JavaScript snippet to disable the popup after this date.

````javascript
(function ($) {
  $(document).ready(function () {
  	const currentDate = new Date();
    const stopDate = new Date('1970-01-01T00:00Z');
    
    if (currentDate.getTime() <= stopDate.getTime()) {
    	elementorProFrontend.modules.popup.showPopup({
        id: 99
      });
    }
  });
})(jQuery);
````

## Add Class to Body on Elementor Pages

You can detect, if a page was build with Elementor. Template usage will be ignored!

````php
add_filter('body_class', function ($classes) {
  $post = get_the_ID();
  
  if (get_post_meta($post, '_elementor_edit_mode', true)) {
    $classes[] = 'elementor-page-builder';
  }
  
  return $classes;
});
````

## Remove Google Fonts

````php
add_filter('elementor/frontend/print_google_fonts', '__return_false');
````

## [FIX] Use Font Awesome Icons

````php
add_action('wp_enqueue_scripts', function () {
		// wp_enqueue_style('elementor-icons-fa-solid');
		wp_enqueue_style('elementor-icons-fa-regular');
}, 99);
````

## [FIX] Elementor Buttons

````css
.elementor-widget-button .elementor-button-wrapper {
		display: block;
		line-height: 0;
}
````

## Show Popup only once a day

````javascript
(function ($) {
  	const now = new Date();
  	const cookieTimeout = now.toDateString();

    let clientCookies = {};
    const browserCookies = document.cookie.split('; ');
   
    const popupID = 99;

    for (let i = 0; i < browserCookies.length; i++) {
        const cookieChunks = browserCookies[i].split('=');
        clientCookies[cookieChunks[0]] = cookieChunks[1];
    }
   
   
    if (typeof clientCookies['DAILY_POPUP_CLOSED'] === 'undefined' || cookieTimeout !== clientCookies['DAILY_POPUP_CLOSED']) {
         $(document).ready(function () {
        elementorProFrontend.modules.popup.showPopup({
            id: popupID
        });
    });
    }
   
    $(document).on('elementor/popup/hide', function (event, id) {
        if (popupID === id) {
         document.cookie = 'DAILY_POPUP_CLOSED=' + now.toDateString() + ';';
        }
    });
})(jQuery);
````

## Check if Elementor Editor is active

````php
if (\Elementor\Plugin::$instance->preview->is_preview_mode()) {
  return;
}
````

## Collapse Elementor Accordion Items on Page Load

````javascript
(function () {
  document.addEventListener('DOMContentLoaded', () => {
    const elementorTabTitles = document.querySelectorAll('.elementor-tab-title');
    
    if (0 !== elementorTabTitles.length) {
      setTimeout(function () {
        elementorTabTitles.forEach((elementorTabTitle) => {
          elementorTabTitle.classList.remove('elementor-active');
          elementorTabTitle.nextElementSibling.style.display = 'none';
        });
      }, 500);
    }
  });
})();
````

## Focus Elementor Search Form by Open Popup

````javascript
(function () {
  window.addEventListener('elementor/popup/show', () => {
    const elementorSearchForm = document.querySelector('.elementor-search-form');
    
    if (null !== elementorSearchForm) {
      elementorSearchForm.querySelector('input[type="search"]').focus();
    }
  });
})();
````

## Dynamic Height for Elementor Testimonial Swiper

````javascript
jQuery(document).ready(function ($) {
    const observer = new MutationObserver(() => {
        $('.elementor-testimonial').each(function () {
            $(this).parent().css({height: parseFloat($(this).height()) + Number(60)})
            if ($(this).parent().hasClass('swiper-slide-active')) {
                $(this).parent().parent().parent().css({
                    height: parseFloat($(this).height()) + Number(60),
                    transition: 'height 500ms',
                })
            }
        });
    });

    observer.observe(document.body, {
        attributes: true,
        attributeFilter: ['class'],
        subtree: true,
    });
});
````

## Conditional Logic for Hidden Form Fields

```javascript
(function () {  
    const elementorForm = document.querySelector('.elementor-form');  
  
    if (null === elementorForm) {  
        return;  
    }  
   
    const hiddenFormFieldsCondition = document.querySelector('select[name="form_fields[custom_selector]"]');  
  
    function handleHiddenFormFields() {  
        const hiddenFormFields = [  
            'hidden_selector',  
        ];  
  
        hiddenFormFields.forEach(hiddenFormField => {  
            const elementorFieldGroup = document.querySelector('.elementor-field-group-' + hiddenFormField);  
  
            if (hiddenFormFieldsCondition.value === 'custom' && elementorFieldGroup) {  
                elementorFieldGroup.classList.add('elementor-field-required', 'elementor-mark-required');  
                elementorFieldGroup.style.display = 'flex';  
                elementorFieldGroup.querySelector('input').required = true;  
            } else {  
                elementorFieldGroup.classList.remove('elementor-field-required', 'elementor-mark-required');  
                elementorFieldGroup.style.display = 'none';  
                elementorFieldGroup.querySelector('input').required = false;  
            }  
        });  
    }  
  
    if (hiddenFormFieldsCondition) {  
        handleHiddenFormFields();  
        hiddenFormFieldsCondition.addEventListener('change', handleHiddenFormFields);  
    }  
})();
```

## Widget Hook

```php
add_filter('elementor/widget/render_content', function($content, $widget) {
	if ('widget' === $widget->get_name()) {
		// modify $content
		$widget->get_settings();
	}
	
	return $content;
}, 10, 2);
```

## Add Loading Lazy Attribute to Image Widget

```php
add_filter('elementor/widget/render_content', function($content, $widget) {
    if ('image' === $widget->get_name()) {
        $content = preg_replace('/<img([^>]*)>/i', '<img$1 loading="lazy">', $content);
    }

    return $content;
}, 10, 2);
```

## Set Custom Font Display to Swap

```php
add_filter('elementor_pro/custom_fonts/font_display', function ($current_value, $font_family, $data) {
	return 'swap';
}, 10, 3);
```

Keep in mind, that you must click "Regenerate Files & Data" in Elementor/Tools.