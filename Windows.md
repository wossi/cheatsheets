# Windows

## Windows 10 GodMod

Create a new Folder on Desktop or where ever you want and name it:

````
GodMod.{ED7BA470-8E54-465E-825C-99712043E01C}
````

## Windows Suche

### Wildcards (Platzhalter)

? ersetzt ein Zeichen
\* ersetzt beliebig viele Zeichen
"" exakter Wortlaut (Dateierkennung wird ignoriert!)

### Beispiel:

````
art:=bild + name:~!web
````

…findet alle Bilder in einem Verzeichnis, welche nicht das Wort “web” enthalten.

## Thumbnails in Windows 10 schneller laden

* Explorer öffnen und unter Ansicht die Option "Ausgeblendete Elemente" aktivieren
* zu "C:\Users\USERNAME\AppData\Local\Microsoft\Windows\Explorer" gehen
* im Ordner "Eigenschaften" auswählen und im Tab "Sicherheit" auf "Erweitert" klicken
* neue Berechtigung hinzufügen und auf den Link "Prinzipal auswählen" klicken
* zu verwendender Objektname: "SYSTEM" eingeben und mit OK bestätigen
* Typ: Verweigern, der Rest bleibt auf den Standard-Einstellungen auf "Erweiterte Berechtigungen anzeigen" klicken
* "Löschen" und "Unterordner und Dateien löschen" aktivieren, alles andere deaktivieren und mit OK bestätigen

## Papierkorb reparieren

Es kann vorkommen, dass sich der Papierkorb nicht mehr leeren lässt oder Fehler beim Öffnen verursacht. Um das Problem zu beheben, einfach das CMD öffnen und folgenden Befehl ausführen:

````
rd /s /q C:\$Recycle.bin
````

Dieser Befehl muss für jedes Laufwerk mit dem entsprechenden Buchstaben durchgeführt werden!

## Backup Script with Robocopy

```
@echo off

cls

:start
echo Backup Mode
echo.
echo 1 = Copy
echo 2 = Mirror
echo.

set choice=
set /p choice=Option: 

if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto copy
if '%choice%'=='2' goto mirror

echo.
echo Error: Input not valid. Try again.
echo.

goto start

:copy
robocopy D:\ B:\ /E /XO /XD "System Volume Information" "$Recycle.bin"
goto end

:mirror
robocopy D:\ B:\ /MIR /XD "System Volume Information" "$Recycle.bin"
goto end

:end
pause
```