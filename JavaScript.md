# JavaScript / jQuery

## CSS Transition and Animation Callback

Call a Javascript function as soon as **CSS transition** has ended...

````javascript
$('.selector').one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
	...
});
````

...or, wait until **CSS animation** has run through...

````javascript
$('.selector').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function () {
	...
});
````

## Find all links on a website

Open the developer console of your browser and run the following command:

````javascript
urls = document.querySelectorAll('a'); for (url in urls) console.log(urls[url].href);
````

## Open all external links in a new window

````javascript
var externalLinks = document.querySelectorAll('a');

function handleExternalLinks(e) {
	e.preventDefault();
	window.open(this.href);
}

externalLinks.forEach(function(externalLink) {
	if(externalLink.host !== location.host && externalLink.getAttribute('href').indexOf('javascript:') !== 0) {
		externalLink.addEventListener('click', handleExternalLinks);
	}
});
````

## Smooth Auto Scrolling (jQuery)

If you need a smooth scroll to an anchor link, you can use the following lines of code...

````javascript
(function($) {
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
                
				return false;
			}
		}
	});
})(jQuery);
````

## Logo Animation on Scroll (Zoom-Out, jQuery)

````javascript
(function ($) {
  var selector = {
    stickyHeader: $('.sticky-header').get(0),
    pageLogo: $('.logo').find('img')
  };
  
  function logoAnimation() {
		if (!selector.stickyHeader) {
			return;
		}
    
    var G = window.innerHeight;
		var W = G - selector.stickyHeader.getBoundingClientRect().top;
    var p = Math.round((W / G) * 100);
    
    var logoVisibility = 1 - (p / 100); // Speed: Default
		var logoSize = 1 - ((p / 8) / 100); // Speed: 8x slower then default
		var logoPosition = 0 - (p / 2); // Speed: 2x slower then default
    
    if (p > 0) {
			selector.pageLogo.css({
				opacity: logoVisibility,
				transform: 'scale(' + logoSize + ') translateY(' + logoPosition + '%)'
			});
    } else {
			selector.pageLogo.css({
				opacity: 1,
				transform: 'scale(1) translateY(0%)'
			});
		}
  }
  
  $(document).ready( function() {
		logoAnimation();
	});
  
  $(document).on('scroll', function() {
		logoAnimation();
	});
})(jQuery);
````

## Javascript Browser Check

````javascript
// Google Chrome
window.navigator.userAgent.indexOf('Chrome') > -1
// Microsoft Internet Explorer
window.navigator.userAgent.indexOf('MSIE') > -1 || !!navigator.userAgent.match(/Trident.*rv\:11\./)
// Microsoft Edge
window.navigator.userAgent.indexOf('Edge') > -1
// Apple Safari
window.navigator.userAgent.indexOf('Safari') > -1 && window.navigator.userAgent.indexOf('Chrome') == -1
// Mozilla Firefox
window.navigator.userAgent.indexOf('Firefox') > -1
// Chromium Edge
window.navigator.userAgent.indexOf('Edg/') > -1
````

## Popup with Cookie Timeout (jQuery)

We need a popup. After it has been closed, it should no longer be displayed for the next hour. So the problem could be solved relatively easily.

````javascript
(function ($) {
	var now = new Date();
	var myCookies = {};
	var browserCookies = document.cookie.split('; ');

	for (var i = 0; i < browserCookies.length; i++) {
		var cookieChunks = browserCookies[i].split('=');
		myCookies[cookieChunks[0]] = cookieChunks[1];
	}

	var cookieTimeout = (now.getHours() + 1) + '' + now.getMinutes();

	if ('undefined' !== myCookies['POPUP_CLOSED'] && cookieTimeout >= myCookies['POPUP_CLOSED']) {
		return;
	}

	setTimeout(function () {
		$('.popup').addClass('popup--visible');
	}, 1000);

	$('.popup-button__close').on('click', function () {
		document.cookie = 'POPUP_CLOSED=' + now.getHours() + '' + now.getMinutes() + ';';
		$('.popup').removeClass('popup--visible');
	});
})(jQuery);
````

## Event Countdown (jQuery)

````javascript
(function ($) {
	$(document).ready(function () {
    const eventCountdown = 	$('.event-countdown');
    
    if (null === eventCountdown) {
      return;
    }
    
    let eventTimestamp = eventCountdown.data('event-timestamp');
    
    if (0 === eventTimestamp) {
      return;
    }
    
    const remainingDays = eventCountdown.find('.event-countdown__days').find('.event-countdown__value');
    const remainingHours = eventCountdown.find('.event-countdown__hours').find('.event-countdown__value');
    const remainingMinutes = eventCountdown.find('.event-countdown__minutes').find('.event-countdown__value');
    const remainingSeconds = eventCountdown.find('.event-countdown__seconds').find('.event-countdown__value');
    
    const eventTimer = setInterval(function () {
      if (eventTimestamp > 0) {
      	eventTimestamp = eventTimestamp - 1;
      } else {
      	clearInterval(eventTimer);
      }
      
      remainingDays.html(Math.floor(eventTimestamp / 86400));
      remainingHours.html(Math.floor((eventTimestamp % 86400) / 3600));
      remainingMinutes.html(Math.floor((eventTimestamp % 3600) / 60));
      remainingSeconds.html(Math.floor(eventTimestamp % 60));
    }, 1000);
	));
})(jQuery);
````

## Get URL Parameter

```javascript
const getParameter = (query) => {
  let url = window.location.search.substring(1),
      variables = url.split('&'),
      parameter,
      i;

	for (i = 0; i < variables.length; i++) {
  	parameter = variables[i].split('=');

    if (parameter[0] === query) {
    	return parameter[1] === undefined ? true : decodeURIComponent(parameter[1]);
    }
  }

  return false;
};

getParameter('id');
```

## Share Button for Mobile Devices (jQuery)

```javascript
(function ($) {
  const shareButton = $('.share-button');
  const shareData = {
    title: shareButton.data('title'),
    text: shareButton.data('text'),
    url: shareButton.data('url'),
  }

  shareButton.on('click', () => {
  	if (navigator.share) {
    	navigator.share(shareData).then(() => {
      	console.log('Successful share')
      }).catch((error) => console.log('Error sharing', error));
    }
	});
})(jQuery);
```

## Hide Container on Outside Click

```javascript
const selector = document.querySelector('.container');

document.addEventListener('mouseup', (e) => {
  if (!selector.contains(e.target)) {
    // Hide element
  }
});
```

## Modify File Upload

```javascript
(function () {
    const fileUpload = document.querySelector('input[type="file"]');

    fileUpload.addEventListener('change', () => {
        let filePreview = '';
        let fileList = new DataTransfer();

        for (const file of fileUpload.files) {
            fileList.items.add(file);
        }

        if (fileList.files.length > 0) {
            for (let i = 0; i < fileList.files.length; i++) {
                if (-1 !== fileList.files[i].type.indexOf('image')) {
                    filePreview += '<img src="' + URL.createObjectURL(fileList.files[i]) + '">';
                } else {
                    filePreview += '<span>' + fileList.files[i].name + '</span>';
                }
            }

            document.querySelector('div').innerHTML = filePreview;
        }
    });
})();
```
## Convert URL Parameter as Object (for Strings and Arrays)

```javascript
const getParameterAsObject = (location) => {
	const url = new URL(location),  
    parameter = new URLSearchParams(url.search),  
    object = {};  
  
	for (const [key, value] of parameter) {  
	    if (key.includes('[]')) {  
	        const arrayKey = key.replace('[]', '');  
	        if (!object[arrayKey]) {  
	            object[arrayKey] = [];  
	        }  
	        object[arrayKey].push(value);  
	    } else {  
	        object[key] = value;  
	    }  
	}  
	  
	return object;
}

const paramsObj = getParameterAsObject(window.location);
```
## Convert Object as URL Parameter

```javascript
const getObjectAsParameter = (object) => {
	const parameter = new URLSearchParams();  
  
	for (const key in object) {  
	    if (Object.prototype.hasOwnProperty.call(object, key)) {  
	        const value = object[key];  
	        if (Array.isArray(value)) {  
	            for (const val of value) {  
	                parameter.append(key + '[]', val);  
	            }  
	        } else {  
	            parameter.append(key, value);  
	        }  
	    }  
	}  
	  
	return parameter.toString();
}

const paramsString = getObjectAsParameter(paramsObj);

// window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + paramsString
```
## XML HTTP Request

```javascript
let xmlHttpRequest = new XMLHttpRequest();
let domParser = new DOMParser();

// xmlHttpRequest.responseType = 'json';
xmlHttpRequest.open('GET', '/path/to/file', true);
xmlHttpRequest.onloadstart = () => {
    if (xmlHttpRequest.readyState === xmlHttpRequest.OPENED) {
        // XHR was started
    }
};

xmlHttpRequest.onload = () => {
    if (xmlHttpRequest.readyState === xmlHttpRequest.DONE) {
        if (xmlHttpRequest.status === 200) {
	        // console.log(xmlHttpRequest.response);
            const parsedDomElements = domParser.parseFromString(xmlHttpRequest.responseText, 'text/html');
            console.log(xmlHttpRequest, parsedDomElements);
        }
    }
};

xmlHttpRequest.send(null);
```